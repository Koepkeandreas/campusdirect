import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import {_goToURL} from "../../utils";
import LinearGradient from "react-native-linear-gradient";
import {Button} from "react-native-elements";

const WIDTH = Dimensions.get('window').width;

type Props = {
    Colors: Object,
    onLogout: Function
}

type State = {}

export default class SettingsFooter extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, onLogout} = this.props;
        const {} = this.state;

        return <View style={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10
        }}>
            <Button raised={true}
                    title={'Logout'}
                    onPress={() => onLogout()}
                    titleStyle={{
                        color: Colors.onPrimary
                    }}
                    containerStyle={{
                        marginBottom: 10
                    }}
                    buttonStyle={{
                        width: WIDTH * 0.9,
                        height: 35
                    }}
                    ViewComponent={LinearGradient}
                    linearGradientProps={{
                        colors: [Colors.primaryDark, Colors.primary],
                        start: {x: 1, y: 1},
                        end: {x: 0, y: 0},
                    }}
            />
            <View style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center'
            }}>
                <Text style={{color: Colors.primary}}
                      onPress={() => _goToURL('http://campusdirectmobile.de/')}
                >
                    CampusDirect
                </Text>
                <Text style={{color: Colors.onBackground}}>
                    |
                </Text>
                <Text style={{color: Colors.primary}}
                      onPress={() => _goToURL('http://campusdirectmobile.de/#/impressum')}
                >
                    Impressum
                </Text>
                <Text style={{color: Colors.onBackground}}>
                    |
                </Text>
                <Text style={{color: Colors.primary}}
                      onPress={() => _goToURL('http://campusdirectmobile.de/#/datenschutz')}
                >
                    {'Datenschutz'}
                </Text>
            </View>
        </View>
    }
}
