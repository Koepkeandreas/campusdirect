import React, {Component} from 'react';
import {StatusBar, View} from 'react-native';

import {Styles} from "./../../utils/styles";
import Picture from "../Picture";
import * as _ from "lodash";

type Props = {
    navigation: any,
    screenProps: {
        Colors: Object,
        privacyAccept: boolean,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string,
        bautzenHash: string
    }
}

type State = {}

export default class AuthLoading extends Component<Props, State> {
    state = {};

    componentDidMount(): void {
        this.checkData()
    };

    checkData = () => {
        const {navigation} = this.props;
        const {campusIndex, studyNumber, privacyAccept, campusDualHash, bautzenHash} = this.props.screenProps;

        if (!campusIndex || (_.isString(campusIndex) && campusIndex.length === 0) || !privacyAccept) return navigation.navigate('CampusPicker');
        if (campusIndex === 'ba_bz') {
            if (!bautzenHash || !(_.isString(bautzenHash) && bautzenHash.length > 0)) return navigation.navigate('Login');
        } else {
            if (!studyNumber || (_.isString(studyNumber) && studyNumber.length === 0) || !campusDualHash || (_.isString(campusDualHash) && campusDualHash.length === 0)) return navigation.navigate('Login');
        }
        return navigation.navigate('App');
    };

    render() {
        const {Colors} = this.props.screenProps;

        return (
            <View style={[Styles.flex, Styles.center, {backgroundColor: Colors.background}]}>
                <StatusBar
                    backgroundColor={Colors.primaryDark}
                    barStyle='light-content'
                />
                <Picture type={'logo_foreground_blue'}
                         imgWidth={150}
                         Colors={Colors}
                />
            </View>
        );
    }
}
