import React, {Component} from 'react';
import {Dimensions, Image, Text, TouchableOpacity, View} from 'react-native';

let WIDTH = Dimensions.get('window').width;

export const images = {
    ba_dd: require('../../Logos/ba_dresden_logo.jpg'),
    ba_bz: require('../../Logos/BA_Bautzen_Logo.jpg'),
    ba_gl: require('../../Logos/BA_Glauchau_Logo.jpg'),
    logo_foreground_blue: require('../../Logos/campus_direct_foreground_blue.png'),
    logo_foreground: require('../../Logos/campus_direct_foreground.png'),
    bautzenHash: require('../../Logos/bautzenHash.jpg'),
    mobileHash: require('../../Logos/mobileHash.jpg'),
    baLogo: require('../../Logos/BALogo.png'),
    campusDual: require('../../Logos/CampusDual.png'),
    opal: require('../../Logos/Opal.png')
};

type Props = {
    type: String,
    label?: String,
    style?: any,
    imgWidth?: number,
    labelStyle?: any,
    getNewHeight?: Function,
    Colors: Object,
    onPress?: Function
}

type State = {
    width: any,
    height: any
}

export default class Picture extends Component<Props, State> {
    state = {
        width: 1,
        height: 1
    };

    componentDidMount() {
        const {type} = this.props;
        if (!!images[type]) {
            const {width, height} = Image.resolveAssetSource(images[type]);
            this.setState({width, height})
        }
    };

    render() {
        let {type, label, style, imgWidth, labelStyle, getNewHeight, Colors, onPress} = this.props;
        const {width, height} = this.state;

        if (!imgWidth) imgWidth = WIDTH;

        const newHeight = (Number(imgWidth) * Number(height)) / Number(width);

        const newImgStyle = {
            width: imgWidth,
            height: newHeight,
            backgroundColor: label ? Colors.PictureBackground : null
        };

        if (getNewHeight) getNewHeight(newHeight);

        return onPress
            ? <TouchableOpacity style={style ? style : {}}
                                onPress={onPress}
            >
                {
                    !!images[type]
                        ? <Image source={images[type]}
                                 style={imgWidth ? newImgStyle : {
                                     width: width,
                                     height: height,
                                     backgroundColor: label ? Colors.PictureBackground : null
                                 }}/>
                        : <Text style={labelStyle ? labelStyle : {}}>
                            {label}
                        </Text>
                }
            </TouchableOpacity>
            : <View style={style ? style : {}}>
                {
                    !!images[type]
                        ? <Image source={images[type]}
                                 style={imgWidth ? newImgStyle : {
                                     width: width,
                                     height: height,
                                     backgroundColor: label ? Colors.PictureBackground : null
                                 }}/>
                        : <Text style={labelStyle ? labelStyle : {}}>
                            {label}
                        </Text>
                }
            </View>;
    }
};
