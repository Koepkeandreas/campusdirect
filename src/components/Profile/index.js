import React, {Component} from 'react';
import {ActivityIndicator, Dimensions, View} from 'react-native';
import {fetchCredits, fetchExamStatus, fetchReminder} from "../../utils";
import {Styles} from "../../utils/styles";
import ProfileBody from "../ProfileBody";
import ProfileFooter from "../ProfileFooter";
import AsyncStorage from "@react-native-community/async-storage";
import _ from 'lodash';
import LinearGradient from "react-native-linear-gradient";
import {Button} from "react-native-elements";

const WIDTH = Dimensions.get('window').width;

type Props = {
    navigation: any,
    screenProps: {
        Colors: Object,
        firstStart: boolean,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string
    }
}

type State = {
    profile: Object,
    loading: boolean,
    updating: boolean
}

export default class Profile extends Component<Props, State> {
    state = {
        profile: {},
        loading: true,
        updating: true
    };

    async componentDidMount(): void {
        const {campusDualHash, studyNumber} = this.props.screenProps;

        if (this.state.loading) {
            let profile = await AsyncStorage.getItem('profile');
            if (profile !== null) {
                try {
                    profile = JSON.parse(profile);
                } catch (e) {
                    profile = {};
                }
            } else profile = {};

            this.setState({
                loading: false,
                profile
            });

            if (this.state.updating && !_.isNil(campusDualHash)) {
                const getCredits = () => new Promise((resolve, reject) => {
                    fetchCredits(studyNumber, campusDualHash, (res) => resolve(res), () => reject())
                });

                const getExamStatus = () => new Promise((resolve, reject) => {
                    try {
                        fetchExamStatus(studyNumber, campusDualHash, (res) => {
                                try {
                                    resolve(JSON.parse(res));
                                } catch (e) {
                                    resolve({});
                                }
                            },
                            () => reject())
                    } catch (e) {
                        alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                    }
                });

                const getReminder = () => new Promise((resolve, reject) => {
                    try {
                        fetchReminder(studyNumber, campusDualHash, (res) => {
                            try {
                                resolve(JSON.parse(res))
                            } catch (e) {
                                resolve({})
                            }
                        }, () => reject())
                    } catch (e) {
                        alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                    }
                });

                const credits = await getCredits();
                const examStatus = await getExamStatus();
                const reminder = await getReminder();

                if (!_.isNil(credits) && !_.isNil(examStatus) && !_.isNil(reminder)) {
                    profile = {
                        credits,
                        totalExams: examStatus.EXAMS,
                        failureExams: examStatus.FAILURE,
                        successExams: examStatus.SUCCESS,
                        totalModules: examStatus.MODULES,
                        successModules: examStatus.MBOOKED,
                        semester: parseInt(reminder.SEMESTER) - 1
                    };

                    await AsyncStorage.setItem('profile', JSON.stringify(profile));

                    this.setState({
                        updating: false,
                        profile
                    })
                } else this.setState({
                    updating: false
                })
            }
        }
    }

    render() {
        const {Colors, campusIndex, campusDualHash} = this.props.screenProps;
        let {profile, loading, updating} = this.state;

        if (_.isNil(profile) || _.isEmpty(profile)) {
            profile = {
                credits: 0,
                totalExams: 0,
                failureExams: 0,
                successExams: 0,
                totalModules: 0,
                successModules: 0,
                semester: 0
            }
        }

        return _.isNil(campusDualHash)
            ? <View style={[Styles.flex, {backgroundColor: Colors.background}, Styles.center]}>
                <Button raised={true}
                        title={'Verknüpfen Sie Ihr CampusDual Profil, um Credits, Module und weitere Statistiken hier zu sehen.'}
                        onPress={() => {
                            this.props.navigation.navigate('Settings')
                        }}
                        titleStyle={{
                            color: Colors.onPrimary,
                            marginVertical: 10
                        }}
                        containerStyle={{}}
                        buttonStyle={{
                            width: WIDTH * 0.9
                        }}
                        ViewComponent={LinearGradient}
                        linearGradientProps={{
                            colors: [Colors.primaryDark, Colors.primary],
                            start: {x: 1, y: 1},
                            end: {x: 0, y: 0},
                        }}
                />
            </View>
            : <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
                <ProfileBody Colors={Colors} profile={profile}/>
                <ProfileFooter Colors={Colors} campus={campusIndex}/>
                {
                    updating
                        ? <View style={{
                            position: 'absolute',
                            right: 15,
                            top: 15,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <ActivityIndicator size="small" color={Colors.primaryDark}/>
                        </View>
                        : null
                }
            </View>
    }
}
