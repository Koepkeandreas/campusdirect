import React, {Component} from 'react';
import {View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Plan from "../Plan";
import Reminder from "../Reminder";
import UpdateNewsModal from "../UpdateNewsModal";

type Props = {
    navigation: any,
    screenProps: {
        Colors: Object,
        firstStart: boolean,
        currentVersion: number,
        newVersion: number,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string,
        bautzenHash: string,
        showTips: boolean
    }
}

type State = {
    updateModalVisible: boolean,
    tipVisible: boolean
}

export default class Home extends Component<Props, State> {
    state = {
        updateModalVisible: this.props.screenProps.currentVersion < this.props.screenProps.newVersion,
        tipVisible: false
    };

    render() {
        const {Colors, bautzenHash, campusDualHash, firstStart, campusIndex, newVersion, studyNumber, showTips} = this.props.screenProps;
        const {updateModalVisible} = this.state;

        return <View style={{width: '100%', height: '100%', backgroundColor: Colors.background}}
        >
            <Plan
                Colors={Colors}
                bautzenHash={bautzenHash}
                campusDualHash={campusDualHash}
                studyNumber={studyNumber}
                campusIndex={campusIndex}
                showTips={showTips}
            />
            <Reminder
                Colors={Colors}
                campusDualHash={campusDualHash}
                studyNumber={studyNumber}
                navigation={this.props.navigation}
            />
            {
                !firstStart
                    ? <UpdateNewsModal visible={updateModalVisible}
                                       onClose={async () => {
                                           await AsyncStorage.setItem('currentVersion', String(newVersion));
                                           this.setState({updateModalVisible: false})
                                       }}
                                       Colors={Colors}
                                       campusIndex={campusIndex}
                    />
                    : null
            }
        </View>;
    }
}
