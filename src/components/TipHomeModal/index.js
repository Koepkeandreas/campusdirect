import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import Modal from "react-native-modal";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

let {width, height} = Dimensions.get('window');

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object,
}

type State = {}

export default class TipHomeModal extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {visible, onClose, Colors} = this.props;
        const {} = this.state;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
        >
            <View style={{
                position: 'absolute',
                top: 10,
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 1,
                backgroundColor: Colors.background,
                borderColor: Colors.border,
                padding: 5
            }}>
                <FontAwesome5 name={'info'}
                              size={15}
                              style={{
                                  paddingRight: 20
                              }}
                              color={Colors.onBackground}/>
                <Text style={{
                    color: Colors.onBackground,
                    width: '80%',
                    textAlign: 'center'
                }}>
                    Du kannst die Tips in den Einstellungen ausschalten.
                </Text>
            </View>
            <View style={{
                position: 'absolute',
                top: height * 0.2,
                flexDirection: 'row',
                width: '40%',
                alignItems: 'center'
            }}>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Auf dem Startbildschirm und in der Wochenansicht, kannst du auf Einträge klicken, um eine
                    Detailansicht zu öffnen.
                </Text>
                <FontAwesome5 name={'arrow-right'}
                              size={35}
                              style={{
                                  paddingLeft: 10
                              }}
                              color={Colors.onBackground}/>
            </View>
            <View style={{
                position: 'absolute',
                bottom: 70,
                width: '100%',
                alignItems: 'center'
            }}>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Im Reminder kannst du deine bevorstehenden Termine, deine letzten Ergebnisse, Wahlmodule und
                    Anmeldungen für Prüfungen sehen. Sollte eine Zahl rot erscheinen, ist ein neuer Eintrag hinzu
                    gekommen.
                </Text>
                <FontAwesome5 name={'arrow-down'}
                              size={35}
                              style={{
                                  paddingTop: 10
                              }}
                              color={Colors.onBackground}/>
            </View>
        </Modal>
    }
}
