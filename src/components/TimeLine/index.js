import React, {Component} from 'react';
import {View} from 'react-native';
import {timeLine} from "../../utils";
import * as _ from "lodash";

type Props = {
    Colors: Object,
    height: number
}

type State = {}

export default class TimeLine extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, height} = this.props;
        const {} = this.state;

        const timeNow = Math.round(new Date().getTime() / 1000);
        const dayStartTime = Math.round(new Date().setHours(_.first(timeLine), 0, 0) / 1000);
        const dayEndTime = Math.round(new Date().setHours(_.last(timeLine) + 1, 0, 0) / 1000);
        const dayTimeRange = dayEndTime - dayStartTime;
        const timeNowTop = (timeNow - dayStartTime) * height / dayTimeRange;

        return timeNow > dayStartTime && timeNow < dayEndTime
            ? <View style={{
                height: 50,
                width: '100%',
                borderTopColor: Colors.onBackground,
                borderTopWidth: 1.5,
                position: 'absolute',
                top: timeNowTop
            }}/>
            : null
    }
}