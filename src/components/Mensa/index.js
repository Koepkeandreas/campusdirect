import React, {Component} from 'react';
import {ToastAndroid, View} from 'react-native';
import {DefaultMensaList, fetchMensa} from "../../utils";
import AsyncStorage from '@react-native-community/async-storage';
import * as _ from "lodash";
import {Styles} from "../../utils/styles";
import MensaHeader from "../MensaHeader";
import MensaBody from "../MensaBody";
import MensaPicker from "../MensaPicker";
import dayjs from 'dayjs';
import PDFView from 'react-native-view-pdf';
import GetFetch from "../../Native-Modules/GetFetch";

type Props = {
    screenProps: {
        Colors: Object,
        mensa: number,
        firstStart: boolean,
        mensaList: Array
    }
}

type State = {
    loading: boolean,
    meals: Array,
    date: any,
    mensa: number,
    pdfLink: string,
    mensaList: Array
}

export default class Mensa extends Component<Props, State> {
    state = {
        loading: true,
        meals: [],
        date: dayjs(),
        mensa: this.props.screenProps.mensa,
        pdfLink: null,
        mensaList: this.props.screenProps.mensaList
    };

    componentDidMount(): void {
        if (this.props.screenProps.firstStart) {
            this.updateList();
        }
        if (this.state.loading) {
            this.update();
        }
    };

    componentDidUpdate(): void {
        if (this.state.loading) {
            this.update();
        }
    };

    updateList = () => {
        GetFetch.request(
            `https://api.studentenwerk-dresden.de/openmensa/v2/canteens`,
            (e) => {
                alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            async (res) => {
                try {
                    let data = JSON.parse(res);

                    const arr = _.filter(DefaultMensaList, (el) => {
                        return _.findIndex(data, {id: el.id}) === -1
                    });

                    data = [
                        ...data,
                        ...arr
                    ];

                    await AsyncStorage.setItem('mensaList', JSON.stringify(data));

                    this.setState({
                        mensaList: data
                    });
                } catch (e) {
                    alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                }
            }
        );
    };

    update = () => {
        let {mensa, date, mensaList} = this.state;

        let pdfLink = null;
        try {
            pdfLink = _.find(mensaList, {id: mensa}, {}).pdfLink;
        } catch (e) {
            pdfLink = null;
        }

        if (_.isNil(pdfLink) && mensa < 1000 && mensa > -1) {
            fetchMensa(mensa, date.valueOf(), (res) => {
                try {
                    const data = JSON.parse(res);

                    this.setState({
                        meals: data,
                        loading: false
                    });
                } catch (e) {
                    alert('Die MensaID ist falsch, wende dich an den Entwickler.');
                    this.setState({
                        meals: [],
                        loading: false
                    });
                }
            });
        }
        if (!_.isNil(pdfLink) && mensa >= 1000) {
            this.setState({
                meals: [],
                pdfLink: pdfLink,
                loading: false
            })
        }

    };

    render() {
        const {Colors} = this.props.screenProps;
        const {loading, date, meals, mensa, pdfLink, mensaList} = this.state;

        return <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
            <MensaPicker
                mensa={mensa}
                mensaList={mensaList}
                onChange={async (mensa) => {
                    await AsyncStorage.setItem('mensa', String(mensa));
                    this.setState({
                        meals: [],
                        pdfLink: null,
                        mensa,
                        loading: true,
                        date: dayjs()
                    })
                }}
                Colors={Colors}
                onListUpdate={() => {
                    this.updateList();
                    ToastAndroid.show('Liste der Mensen wurde aktualisiert!', ToastAndroid.SHORT);
                }}
            />
            {
                pdfLink !== null
                    ? <View style={[Styles.flex]}>
                        <PDFView
                            fadeInDuration={250.0}
                            style={{flex: 1, backgroundColor: Colors.background}}
                            resource={pdfLink}
                            resourceType={'url'}
                        />
                    </View>
                    : <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
                        <MensaHeader Colors={Colors}
                                     date={date}
                                     right={() => this.setState({
                                         date: date.add(1, 'day'),
                                         loading: true
                                     })}
                                     left={() => this.setState({
                                         date: date.subtract(1, 'day'),
                                         loading: true
                                     })}
                                     middle={() => this.setState({
                                         date: dayjs(),
                                         loading: true
                                     })}
                        />
                        <MensaBody Colors={Colors}
                                   meals={meals}
                                   loading={loading}
                        />
                    </View>
            }
        </View>
    }
}
