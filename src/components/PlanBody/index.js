import React, {Component} from 'react';
import {View} from 'react-native';
import PlanTimeColumn from "../PlanTimeColumn";
import PlanBodyCalendar from "../PlanBodyCalendar";
import {getNearestDate} from "../../utils";

type Props = {
    Colors: Object,
    plan: Array
}

type State = {
    height: number,
    width: number
}

export default class PlanBody extends Component<Props, State> {
    state = {
        height: 0.1,
        width: 0.1
    };

    componentDidMount(): void {

    }

    render() {
        const {Colors, plan} = this.props;
        const {height, width} = this.state;

        return <View style={
            {
                flex: 1,
                flexDirection: 'row',
                marginHorizontal: 10,
                marginBottom: 10,
                borderBottomWidth: 1,
                borderRightWidth: 1,
                borderLeftWidth: 1,
                borderBottomEndRadius: 5,
                borderBottomRightRadius: 5,
                borderBottomStartRadius: 5,
                borderBottomLeftRadius: 5,
                borderColor: Colors.border,
                backgroundColor: Colors.PlanBackgroundTrans
            }}
                     onLayout={(event) => {
                         let {x, y, width, height} = event.nativeEvent.layout;
                         this.setState({height, width})
                     }}
        >
            <PlanBodyCalendar
                Colors={Colors}
                height={height}
                showTimeLine={true}
                showHalfLine={true}
                showItems={true}
                plan={plan}
                date={new Date()}
            />
            <PlanTimeColumn
                Colors={Colors}
                showTimeline={true}
                height={height}
                width={30}
            />
            <PlanBodyCalendar
                Colors={Colors}
                height={height}
                showTimeLine={false}
                showHalfLine={true}
                showItems={true}
                plan={plan}
                date={getNearestDate(plan)}
            />
        </View>
    }
}
