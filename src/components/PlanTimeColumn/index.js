import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {timeLine} from "../../utils";
import * as _ from "lodash";
import TimeLine from "../TimeLine";

type Props = {
    Colors: object,
    height: number,
    showTimeline: boolean,
    width: number,
    backgroundColor?: String
}

type State = {}

export default class PlanTimeColumn extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, height, showTimeline, width, backgroundColor} = this.props;
        const {} = this.state;

        return (
            <View style={{
                width: width,
                height: '100%',
                borderRightWidth: 1,
                borderLeftWidth: 1,
                borderColor: Colors.border,
                alignItems: 'center',
                backgroundColor: backgroundColor ? backgroundColor : null
            }}>
                {
                    _.map(timeLine, (el, index) => {
                        const h = height / timeLine.length;

                        return <Text key={index} style={{height: h, color: Colors.onBackground, fontSize: 12}}>
                            {el}
                        </Text>
                    })
                }
                {
                    showTimeline
                        ? <TimeLine
                            Colors={Colors}
                            height={height}
                        />
                        : null
                }
            </View>
        );
    }
}
