import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import Modal from "react-native-modal";
import _ from "lodash";
import LinearGradient from "react-native-linear-gradient";

type Props = {
    visible: boolean,
    onClose: Function,
    data: Array,
    Colors: Object
}

export default class ReminderUpcomingModal extends Component<Props> {
    render() {
        const {visible, onClose, data, Colors} = this.props;

        return (
            <Modal
                backdropOpacity={0.3}
                isVisible={visible}
                onBackdropPress={() => onClose()}
                onBackButtonPress={() => onClose()}
                style={{alignItems: 'center'}}
            >
                <View style={{
                    width: '90%',
                    backgroundColor: Colors.background,
                    shadowOpacity: 5,
                    shadowRadius: 15,
                    borderWidth: 1,
                    borderColor: Colors.border,
                    elevation: 15
                }}>
                    <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                    style={{
                                        height: 30,
                                        justifyContent: 'center',
                                        backgroundColor: Colors.primary
                                    }}
                    >
                        <Text style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            color: Colors.onPrimary,
                            paddingLeft: 10
                        }}>
                            {'Termine'}
                        </Text>
                    </LinearGradient>
                    <View style={{
                        padding: 5,
                        maxHeight: 200
                    }}>
                        <ScrollView>
                            <View style={{
                                width: '100%'
                            }}>
                                {
                                    _.map(data, (el, index) => {
                                        const evdat = _.get(el, 'EVDAT', '');
                                        const beguz = _.get(el, 'BEGUZ', '');
                                        const enduz = _.get(el, 'ENDUZ', '');

                                        const year = evdat.substring(0, 4);
                                        const month = evdat.substring(4, 6);
                                        const day = evdat.substring(6, 8);
                                        const startHour = beguz.substring(0, 2);
                                        const startMinute = beguz.substring(2, 4);
                                        const endHour = enduz.substring(0, 2);
                                        const endMinute = enduz.substring(2, 4);

                                        return <View key={index} style={{
                                            padding: 10,
                                            borderBottomWidth: data.length === index + 1 ? 0 : 0.2,
                                        }}>
                                            <Text numberOfLines={1} style={{
                                                fontSize: 16,
                                                fontWeight: 'bold',
                                                color: Colors.onBackground
                                            }}>
                                                {_.get(el, 'SM_STEXT', '')}
                                            </Text>
                                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <Text numberOfLines={1} style={{
                                                    fontSize: 16,
                                                    color: Colors.onBackground
                                                }}>
                                                    {day}.{month}.{year}
                                                </Text>
                                                <Text numberOfLines={1} style={{
                                                    fontSize: 16,
                                                    color: Colors.onBackground
                                                }}>
                                                    {startHour}:{startMinute} - {endHour}:{endMinute}
                                                </Text>
                                            </View>
                                            {
                                                el.SROOM
                                                    ? <Text numberOfLines={1} style={{
                                                        fontSize: 16,
                                                        color: Colors.onBackground
                                                    }}>
                                                        {el.SROOM}
                                                    </Text>
                                                    : null
                                            }
                                        </View>
                                    })
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        );
    }
}

