import React, {Component} from 'react';
import {BackHandler, Dimensions, View} from 'react-native';
import _ from "lodash";
import Icon from "react-native-vector-icons/FontAwesome5";

import DismissKeyboardView from "./../DismissKeyboard";
import Picture from "../Picture";
import Form from "../Form";
import LoginCampusDualHashInfoModal from "../LoginCampusDualHashInfoModal";
import LoginBautzenHashInfoModal from "../LoginBautzenHashInfoModal";
import {
    encryptBautzenHash,
    encryptCampusDualHash,
    encryptStudyNumber,
    testBautzenLogin,
    testCampusDualLogin
} from '../../utils';
import {Styles} from "../../utils/styles";
import LinearGradient from "react-native-linear-gradient";
import AsyncStorage from "@react-native-community/async-storage";
import QrCodeModal from "../QrCodeModal";

let {width} = Dimensions.get('window');

type Props = {
    navigation: any,
    screenProps: {
        Colors: Object,
        campusIndex: String,
        studyNumber: String,
        campusDualHash: String,
        bautzenHash: String,
        reload: Function,
        ts: string,
        deviceID: string
    }
};

type State = {
    studyNumber: String,
    campusDualHash: String,
    bautzenHash: String,
    campusDualHashInfoModalVisible: boolean,
    bautzenHashInfoModalVisible: boolean,
    qrCodeModalVisible: boolean
};

export default class Login extends Component<State, Props> {
    state = {
        studyNumber: '',//3003054   lea:3003824   elina:1001036
        campusDualHash: '',//064e28dbed5fb761fd66b16d97a20f9d      lea:3e9ef2728df919cbb79893b64d3173d0   elina:7af3c772ffd0ce444ac6c77c09553dcf
        bautzenHash: '',//cd2adb40d16204b70b996ac9137e9c650f4a4f772259cce5
        campusDualHashInfoModalVisible: false,
        bautzenHashInfoModalVisible: false,
        qrCodeModalVisible: false
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBack);
    }

    handleBack = () => {
        this.props.navigation.navigate('CampusPicker');
        return true;
    };

    login = () => {
        const {studyNumber, bautzenHash, campusDualHash} = this.state;
        const {campusIndex, reload, ts, deviceID} = this.props.screenProps;

        if (campusIndex === 'ba_bz') {
            if (_.isNil(bautzenHash) || bautzenHash.length === 0) return alert('Bitte gib deinen Bautzen-Hash ein');

            testBautzenLogin(bautzenHash,
                async (res) => {
                    try {
                        const data = JSON.parse(res);
                        if (_.isArray(data)) {
                            const encryptedBautzenHash = await encryptBautzenHash(bautzenHash, ts);

                            await AsyncStorage.setItem('encryptedBautzenHash', encryptedBautzenHash);
                            reload();
                        } else alert('Falscher Bautzen Hash');
                    } catch (e) {
                        alert('Falscher Bautzen Hash');
                    }
                }
            );
        } else {
            if (!studyNumber || studyNumber.length === 0) return alert('Bitte gib deine Matrikelnummer ein');
            if (!campusDualHash || campusDualHash.length === 0) return alert('Bite gib deinen CampusDual-Hash ein');

            testCampusDualLogin(studyNumber,
                campusDualHash,
                async (response) => {
                    try {
                        const res = _.toNumber(response);
                        if (!_.isNaN(res)) {
                            const encryptedStudyNumber = await encryptStudyNumber(studyNumber, ts);
                            const encryptedCampusDualHash = await encryptCampusDualHash(campusDualHash, studyNumber, ts);

                            await AsyncStorage.setItem('encryptedCampusDualHash', encryptedCampusDualHash);
                            await AsyncStorage.setItem('encryptedStudyNumber', encryptedStudyNumber);
                            reload();
                        } else {
                            alert('Falscher Campus Dual Hash oder falsche Matrikelnummer')
                        }
                    } catch (e) {
                        alert(e)
                    }
                }
            );
        }
    };

    render() {
        const {studyNumber, campusDualHash, bautzenHash, campusDualHashInfoModalVisible, bautzenHashInfoModalVisible, qrCodeModalVisible} = this.state;
        const {Colors, campusIndex} = this.props.screenProps;

        const loginAttributes = [];

        if (campusIndex === 'ba_bz') {
            loginAttributes.push({
                type: 'textInput',
                placeholder: 'Bautzen Hash',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Bautzen Hash',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (bautzenHash) => this.setState({bautzenHash}),
                defaultValue: bautzenHash,
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                rightIcon: <Icon
                    name='question-circle'
                    size={25}
                    color={Colors.onBackground}
                    onPress={() => this.setState({bautzenHashInfoModalVisible: true})}
                />,
                leftIcon: <Icon
                    name='qrcode'
                    size={25}
                    style={{
                        marginRight: 10
                    }}
                    color={Colors.onBackground}
                    onPress={() => this.setState({qrCodeModalVisible: true})}
                />
            });
        } else {
            loginAttributes.push(
                {
                    type: 'textInput',
                    placeholder: 'Matrikel Nr',
                    placeholderTextColor: Colors.onBackgroundTrans,
                    label: 'Matrikel Nr',
                    labelStyle: {
                        color: Colors.onBackgroundTrans,
                        fontSize: 12
                    },
                    onChange: (studyNumber) => this.setState({studyNumber}),
                    defaultValue: studyNumber,
                    inputStyle: {
                        color: Colors.onBackground,
                        fontSize: 16
                    },
                    containerStyle: {
                        width: '80%',
                        minWidth: 250,
                        marginBottom: 20
                    }
                },
                {
                    type: 'textInput',
                    placeholder: 'CampusDual Hash',
                    placeholderTextColor: Colors.onBackgroundTrans,
                    label: 'CampusDual Hash',
                    labelStyle: {
                        color: Colors.onBackgroundTrans,
                        fontSize: 12
                    },
                    onChange: (campusDualHash) => this.setState({campusDualHash}),
                    defaultValue: campusDualHash,
                    containerStyle: {
                        width: '80%',
                        minWidth: 250,
                        marginBottom: 20
                    },
                    inputStyle: {
                        color: Colors.onBackground,
                        fontSize: 16
                    },
                    rightIcon: <Icon
                        name='question-circle'
                        size={25}
                        color={Colors.onBackground}
                        onPress={() => this.setState({campusDualHashInfoModalVisible: true})}
                    />
                }
            );
        }

        loginAttributes.push({
            type: 'submit',
            placeholder: 'LOGIN',
            raised: true,
            containerStyle: {
                width: '80%',
                marginTop: 10,
                minWidth: 250
            },
            buttonStyle: {backgroundColor: Colors.primary},
            titleStyle: {color: Colors.onPrimary},
            onSubmit: () => this.login(),
            ViewComponent: LinearGradient,
            linearGradientProps: {
                colors: [Colors.primaryDark, Colors.primary],
                start: {x: 1, y: 1},
                end: {x: 0, y: 0},
            }
        });

        return <View style={{flex: 1, backgroundColor: Colors.background}}>
            <DismissKeyboardView style={[Styles.flex, Styles.center, {backgroundColor: Colors.background}]}>
                <View style={{
                    height: '75%',
                    minHeight: 500,
                    width: '100%',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                    <Picture type={campusIndex}
                             style={{alignItems: 'center', width: width * 0.7, marginVertical: 50}}
                             imgWidth={width * 0.7}
                             textStyle={{fontSize: 26, color: Colors.onBackground, fontWeight: 'bold'}}
                             Colors={Colors}
                    />
                    <Form form={loginAttributes}
                          formStyle={{alignItems: 'center', width: '100%'}}
                    />
                </View>
            </DismissKeyboardView>
            <Icon name={'arrow-left'}
                  size={25}
                  color={Colors.primary}
                  onPress={() => this.handleBack()}
                  style={{position: 'absolute', margin: 20}}
            />
            <LoginCampusDualHashInfoModal visible={campusDualHashInfoModalVisible}
                                          onClose={() => this.setState({campusDualHashInfoModalVisible: false})}
                                          Colors={Colors}
            />
            <LoginBautzenHashInfoModal visible={bautzenHashInfoModalVisible}
                                       onClose={() => this.setState({bautzenHashInfoModalVisible: false})}
                                       Colors={Colors}
            />
            {
                qrCodeModalVisible
                    ? <QrCodeModal Colors={Colors}
                                   onClose={() => this.setState({qrCodeModalVisible: false})}
                                   onRead={(code) => {
                                       this.setState({bautzenHash: code.data, qrCodeModalVisible: false})
                                   }}
                    />
                    : null
            }

        </View>;
    }
};
