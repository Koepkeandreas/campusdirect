import React, {Component} from 'react';
import {Text, View} from 'react-native';

type Props = {
    label: String,
    data: any,
    Colors: Object
}

type State = {}

export default class ProfileBodyDataView extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, label, data} = this.props;
        const {} = this.state;

        const styles = {
            container: {
                width: '100%',
                height: '30%',
                backgroundColor: Colors.PlanBackground,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: Colors.border,
                marginVertical: 5
            },
            label: {
                height: 25,
                marginLeft: 5,
                width: '100%',
                justifyContent: 'center'
            },
            labelText: {
                fontSize: 14,
                color: Colors.onBackground
            },
            body: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            },
            bodyText: {
                fontSize: 20,
                fontWeight: 'bold',
                color: Colors.onBackground
            }
        };

        return <View style={styles.container}>
            <View style={styles.label}>
                <Text style={styles.labelText}>
                    {label}
                </Text>
            </View>
            <View style={styles.body}>
                <Text style={styles.bodyText}>
                    {data}
                </Text>
            </View>
        </View>
    }
}