import React, {Component} from 'react';
import {RefreshControl, ScrollView, View} from 'react-native';
import * as _ from "lodash";
import GestureRecognizer from "react-native-swipe-gestures";

import {Styles} from "../../utils/styles";
import PlanTimeColumn from "../PlanTimeColumn";
import {getDateList, timeLine} from "../../utils";
import PlanSmallItem from "../PlanSmallItem";
import WeekPlanBodyTimeline from "../WeekPlanBodyTimeline";
import PlanDetailModal from "../PlanDetailModal";

type Props = {
    Colors: Object,
    weekPlan: Array,
    date: number,
    left: Function,
    right: Function,
    refresh: Function,
    loading: boolean,
    removeCustomEntry: Function,
    editCustomEntry: Function
}

type State = {
    height: number,
    modalItems: Array,
    visible: boolean
}

export default class WeekPlanBody extends Component<Props, State> {
    state = {
        height: 0.1,
        modalItems: [],
        visible: false
    };

    componentDidMount(): void {

    }

    render() {
        const {Colors, weekPlan, left, right, date, loading, refresh} = this.props;
        const {height, modalItems, visible} = this.state;

        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80,
            gestureIsClickThreshold: 10
        };

        const dateList = getDateList(date);

        return <ScrollView
            contentContainerStyle={[
                Styles.flex
            ]}
            refreshControl={
                <RefreshControl refreshing={loading}
                                colors={[Colors.primary]}
                                onRefresh={refresh}/>
            }
        >
            <WeekPlanBodyTimeline Colors={Colors}
                                  dateList={dateList}
            />
            <GestureRecognizer
                onSwipeLeft={() => right()}
                onSwipeRight={() => left()}
                config={config}
                style={{
                    flex: 1,
                    flexDirection: 'row'
                }}
                onLayout={(event) => {
                    let {x, y, width, height} = event.nativeEvent.layout;
                    this.setState({height, width})
                }}
            >
                <PlanTimeColumn
                    Colors={Colors}
                    showTimeline={true}
                    height={height}
                    width={25}
                    backgroundColor={Colors.BackgroundTrans}
                />
                <View style={{
                    flex: 1,
                    flexDirection: 'row'
                }}>
                    {
                        _.map(dateList, (el, index) => {
                            const dayStartTime = Math.round(el.time.setHours(_.first(timeLine), 0, 0) / 1000);
                            const dayEndTime = Math.round(el.time.setHours(_.last(timeLine) + 1, 0, 0) / 1000);
                            const dayTimeRange = dayEndTime - dayStartTime;

                            const h = (dayTimeRange / timeLine.length + 1) * height / dayTimeRange;

                            return <View key={index}
                                         style={{
                                             width: index !== 6 ? '15%' : '10%',
                                             minWidth: index !== 6 ? 40 : 35,
                                             borderRightWidth: 1,
                                             alignItems: 'center',
                                             borderRightColor: Colors.border,
                                             backgroundColor: el.today ? Colors.secondaryLightTrans : Colors.PlanBackgroundTrans
                                         }}
                            >
                                {
                                    _.map(timeLine, (timelineItem, timelineIndex) => <View key={timelineIndex} style={{
                                            width: '100%',
                                            height: h,
                                            borderBottomWidth: timeLine.length === timelineIndex + 1 ? 0 : 0.5,
                                            borderColor: Colors.border,
                                            justifyContent: 'center'
                                        }}/>
                                    )
                                }
                                <PlanSmallItem
                                    Colors={Colors}
                                    plan={weekPlan}
                                    height={height}
                                    date={el.time}
                                    openPlanDetailModal={(items) => this.setState({
                                        modalItems: items,
                                        visible: true
                                    })}
                                />
                            </View>
                        })
                    }
                    <PlanDetailModal Colors={Colors}
                                     visible={visible}
                                     onClose={() => this.setState({visible: false, modalItems: []})}
                                     items={modalItems}
                                     removeCustomEntry={(item) => {
                                         this.setState({
                                             visible: false, modalItems: []
                                         });
                                         this.props.removeCustomEntry(item);
                                     }}
                                     editCustomEntry={(item) => {
                                         this.setState({
                                             visible: false, modalItems: []
                                         });
                                         this.props.editCustomEntry(item);
                                     }}
                    />
                </View>
            </GestureRecognizer>
        </ScrollView>
    }
}
