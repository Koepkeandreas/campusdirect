import React, {Component} from 'react';
import {Text, View} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/FontAwesome5";
import _ from "lodash";
import Form from "../Form";
import LoginCampusDualHashInfoModal from "../LoginCampusDualHashInfoModal";
import {encryptCampusDualHash, encryptStudyNumber, testCampusDualLogin} from "../../utils";
import AsyncStorage from "@react-native-community/async-storage";

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object,
    reload: Function,
    ts: number
}

type State = {
    studyNumber: string,
    campusDualHash: string,
    campusDualHashInfoModalVisible: boolean
}

export default class CampusDualLinkModal extends Component<Props, State> {
    state = {
        studyNumber: '',
        campusDualHash: '',
        campusDualHashInfoModalVisible: false
    };

    componentDidMount(): void {

    }

    cancel = () => {
        const {onClose} = this.props;

        this.setState({
            studyNumber: '',
            campusDualHash: '',
            campusDualHashInfoModalVisible: false
        });

        onClose();
    };

    login = () => {
        const {ts} = this.props;
        const {studyNumber, campusDualHash} = this.state;

        if (!studyNumber || studyNumber.length === 0) return alert('Bitte gib deine Matrikelnummer ein');
        if (!campusDualHash || campusDualHash.length === 0) return alert('Bite gib deinen CampusDual-Hash ein');

        testCampusDualLogin(studyNumber,
            campusDualHash,
            async (response) => {
                try {
                    const res = _.toNumber(response);
                    if (!_.isNaN(res)) {
                        const encryptedStudyNumber = await encryptStudyNumber(studyNumber, ts);
                        const encryptedCampusDualHash = await encryptCampusDualHash(campusDualHash, studyNumber, ts);

                        await AsyncStorage.setItem('encryptedCampusDualHash', encryptedCampusDualHash);
                        await AsyncStorage.setItem('encryptedStudyNumber', encryptedStudyNumber);
                        this.props.reload();
                    } else {
                        alert('Falscher Campus Dual Hash oder falsche Matrikelnummer')
                    }
                } catch (e) {
                    alert(e)
                }
            }
        );
    };

    render() {
        const {Colors, visible} = this.props;
        const {studyNumber, campusDualHash, campusDualHashInfoModalVisible} = this.state;

        const loginAttributes = [
            {
                type: 'textInput',
                placeholder: 'Matrikel Nr',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Matrikel Nr',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (studyNumber) => this.setState({studyNumber}),
                keyboardType: 'numeric',
                defaultValue: studyNumber,
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                }
            },
            {
                type: 'textInput',
                placeholder: 'CampusDual Hash',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'CampusDual Hash',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (campusDualHash) => this.setState({campusDualHash}),
                defaultValue: campusDualHash,
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                rightIcon: <Icon
                    name='question-circle'
                    size={25}
                    color={Colors.onBackground}
                    onPress={() => this.setState({campusDualHashInfoModalVisible: true})}
                />
            }, {
                type: 'submit',
                placeholder: 'Verknüpfen',
                raised: true,
                containerStyle: {
                    width: '80%',
                    marginTop: 10,
                    minWidth: 250
                },
                buttonStyle: {backgroundColor: Colors.primary},
                titleStyle: {color: Colors.onPrimary},
                onSubmit: () => this.login(),
                ViewComponent: LinearGradient,
                linearGradientProps: {
                    colors: [Colors.primaryDark, Colors.primary],
                    start: {x: 1, y: 1},
                    end: {x: 0, y: 0},
                }
            }
        ];

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={this.cancel}
            onBackButtonPress={this.cancel}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.background,
                borderRadius: 3,
                shadowOpacity: 15,
                elevation: 15,
                marginBottom: 5,
                marginTop: 5,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 25,
                                    justifyContent: 'space-between',
                                    backgroundColor: Colors.primary,
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderColor: Colors.primary
                                }}
                >
                    <Text style={{
                        color: Colors.onPrimary,
                        fontWeight: 'bold',
                        fontSize: 16,
                        paddingLeft: 10
                    }}>
                        CampusDual verknüpfen
                    </Text>
                </LinearGradient>
                <View style={{
                    backgroundColor: Colors.background,
                    padding: 10
                }}>
                    <Form form={loginAttributes}
                          formStyle={{alignItems: 'center', width: '100%'}}
                    />
                </View>
                <LoginCampusDualHashInfoModal visible={campusDualHashInfoModalVisible}
                                              onClose={() => this.setState({campusDualHashInfoModalVisible: false})}
                                              Colors={Colors}
                />
            </View>
        </Modal>
    }
}
