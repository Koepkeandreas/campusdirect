import React, {Component} from 'react';
import {Dimensions, View} from 'react-native';
import Picture from "../Picture";
import {_goToURL, CampusLinklist} from "../../utils";

const WIDTH = Dimensions.get('window').width;

type Props = {
    Colors: Object,
    campus: String
}

type State = {}

export default class ProfileFooter extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, campus} = this.props;
        const {} = this.state;

        const campusLink = CampusLinklist[campus];

        return <View style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'flex-end',
            marginVertical: 10
        }}>
            <Picture type={'baLogo'}
                     imgWidth={WIDTH * 0.2}
                     Colors={Colors}
                     onPress={() => _goToURL(campusLink)}
            />
            <Picture type={'campusDual'}
                     imgWidth={WIDTH * 0.4}
                     Colors={Colors}
                     onPress={() => _goToURL('https://erp.campus-dual.de/sap/bc/webdynpro/sap/zba_initss?sap-client=100&sap-language=de&uri=https://selfservice.campus-dual.de/index/login')}
            />
            <Picture type={'opal'}
                     imgWidth={WIDTH * 0.2}
                     Colors={Colors}
                     onPress={() => _goToURL('https://bildungsportal.sachsen.de/opal/login')}
            />
        </View>
    }
}
