import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import LinearGradient from "react-native-linear-gradient";
import {Button} from "react-native-elements";

const SCREEN_HEIGHT = Dimensions.get("window").height;

type Props = {
    Colors: Object,
    onClose: Function,
    onRead: Function
}

type State = {}

export default class QrCodeModal extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {onClose, visible, Colors, onRead} = this.props;
        const {} = this.state;

        return <View style={{position: 'absolute', height: '100%', width: '100%', backgroundColor: Colors.background}}>
            <QRCodeScanner
                cameraStyle={{height: SCREEN_HEIGHT}}
                onRead={onRead}
                showMarker={true}
            />
            <View style={{width: '100%', height: 100, alignItems: 'center'}}>
                <Button raised={true}
                        title={'Abbrechen'}
                        onPress={() => {
                            onClose()
                        }}
                        titleStyle={{
                            color: Colors.onPrimary
                        }}
                        containerStyle={{
                            width: 150,
                            margin: 5
                        }}
                        buttonStyle={{
                            height: 35
                        }}
                        ViewComponent={LinearGradient}
                        linearGradientProps={{
                            colors: [Colors.primaryDark, Colors.primary],
                            start: {x: 1, y: 1},
                            end: {x: 0, y: 0},
                        }}
                />
            </View>
            <View style={{
                position: 'absolute',
                width: '100%',
                height: 100,
                alignItems: 'center',
                justifyContent: 'flex-end'
            }}>
                <Text style={{color: 'white', fontSize: 26, fontWeight: 'bold'}}>
                    Scanne deinen Bautzen QR Code!
                </Text>
            </View>
        </View>
    }
}
