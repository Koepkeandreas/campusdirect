import React, {Component} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {fetchPlan} from "../../utils";
import * as _ from "lodash";
import WeekPlanHeader from "../WeekPlanHeader";
import WeekPlanBody from "../WeekPlanBody";
import {Styles} from "../../utils/styles";
import AsyncStorage from "@react-native-community/async-storage";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AddPlanEntryModal from "../AddPlanEntryModal";
import TipWeekModal from "../TipWeekModal";
import dayjs from "../Plan";

type Props = {
    screenProps: {
        Colors: Object,
        firstStart: boolean,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string,
        bautzenHash: string,
        showTips: boolean
    }
}

type State = {
    loading: boolean,
    updating: boolean,
    studyPlan: Array,
    date: any,
    addPlanEntryModalVisible: boolean,
    customPlanItem: Object,
    tipVisible: boolean
}

export default class WeekPlan extends Component<Props, State> {
    state = {
        loading: true,
        updating: false,
        studyPlan: [],
        customPlan: [],
        date: new Date().getTime(),
        addPlanEntryModalVisible: false,
        customPlanItem: {},
        tipVisible: false
    };

    async componentDidMount(): void {
        if (this.state.loading) {
            let customPlan = await AsyncStorage.getItem('customPlan');
            if (customPlan !== null) {
                try {
                    customPlan = JSON.parse(customPlan);
                } catch (e) {
                    customPlan = [];
                }
            } else customPlan = [];

            let studyPlan = await AsyncStorage.getItem('studyPlan');
            if (studyPlan !== null) {
                try {
                    studyPlan = JSON.parse(studyPlan);
                } catch (e) {
                    studyPlan = [];
                }
            } else studyPlan = [];

            let studyPlanFilter = await AsyncStorage.getItem('studyPlanFilter');
            if (studyPlanFilter !== null) {
                try {
                    studyPlanFilter = JSON.parse(studyPlanFilter);
                } catch (e) {
                    studyPlanFilter = [];
                }
            }

            if (studyPlanFilter && _.isArray(studyPlanFilter) && studyPlanFilter.length > 0) {
                studyPlan = _.filter(studyPlan, (entry: Object) => _.indexOf(studyPlanFilter, entry.title) !== -1)
            }

            this.setState({
                loading: false,
                customPlan,
                studyPlan,
                tipVisible: this.props.screenProps.showTips
            });
        }
    };

    async componentDidUpdate(): void {
        if (this.state.updating) {
            const {bautzenHash, campusDualHash, campusIndex, studyNumber} = this.props.screenProps;

            let studyPlanFilter = await AsyncStorage.getItem('studyPlanFilter');
            if (studyPlanFilter !== null) {
                try {
                    studyPlanFilter = JSON.parse(studyPlanFilter);
                } catch (e) {
                    studyPlanFilter = [];
                }
            }

            fetchPlan(campusIndex, studyNumber, campusDualHash, bautzenHash, this.state.date, async (plan: string) => {
                try {
                    let studyPlan = JSON.parse(plan);

                    studyPlan = _.map(studyPlan, (el) => {
                        if(typeof el.start === 'string' && el.start.match(/[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}/gm)){
                            el.start = dayjs(el.start, 'YYYY-MM-DD HH:mm').unix();
                            el.end = dayjs(el.end, 'YYYY-MM-DD HH:mm').unix();
                        }

                        return el;
                    });

                    await AsyncStorage.setItem('studyPlan', JSON.stringify(studyPlan));


                    let oldModuleList = await AsyncStorage.getItem('studyPlanModuleList');
                    if (oldModuleList !== null) {
                        try {
                            oldModuleList = JSON.parse(oldModuleList);
                        } catch (e) {
                            oldModuleList = [];
                        }
                    }
                    const studyPlanModuleList = _.uniq(_.map(studyPlan, (entry) => entry.title));

                    if (studyPlanFilter === null || studyPlanFilter.length === 0) {
                        studyPlanFilter = studyPlanModuleList;
                    } else {
                        const newModule = _.filter(studyPlanModuleList, (el) => _.indexOf(oldModuleList, el) === -1);
                        _.map(newModule, (el) => {
                            studyPlanFilter.push(el)
                        });
                    }

                    if (studyPlanFilter && _.isArray(studyPlanFilter) && studyPlanFilter.length > 0) {
                        studyPlan = _.filter(studyPlan, (entry: Object) => _.indexOf(studyPlanFilter, entry.title) !== -1);
                        studyPlan = _.sortBy(studyPlan, 'start', 'desc');
                    }

                    this.setState({
                        updating: false,
                        studyPlan
                    })
                } catch (e) {
                    alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                }
            }, () => {
                this.setState({
                    updating: false
                })
            })
        }
    };

    onAddItem = async (data) => {
        let {customPlan, studyPlan} = this.state;

        const multiEntry = _.filter(studyPlan, el => {
            return (el.start <= data.end && data.end <= el.end) || (el.start <= data.start && data.start <= el.end)
        });

        if (multiEntry.length === 0) {
            customPlan.push(data);

            await AsyncStorage.setItem('customPlan', JSON.stringify(customPlan));

            this.setState({
                customPlan,
                addPlanEntryModalVisible: false
            })
        } else {
            alert('Der gewählte Eintrag überlappt sich mit einem anderen Eintrag. Bitte wähle eine andere Zeit.');
        }
    };

    onSaveItem = async (data) => {
        let {customPlan, customPlanItem, studyPlan} = this.state;

        const multiEntry = _.filter(studyPlan, el => {
            return (el.start <= data.end && data.end <= el.end) || (el.start <= data.start && data.start <= el.end)
        });

        if (multiEntry.length === 0) {
            _.remove(customPlan, customPlanItem);
            customPlan.push(data);

            await AsyncStorage.setItem('customPlan', JSON.stringify(customPlan));

            this.setState({
                customPlan,
                customPlanItem: {},
                addPlanEntryModalVisible: false
            })
        } else {
            alert('Der gewählte Eintrag überlappt sich mit einem anderen Eintrag. Bitte wähle eine andere Zeit.');
        }
    };

    removeCustomEntry = async (item) => {
        let {customPlan} = this.state;

        _.remove(customPlan, item);

        await AsyncStorage.setItem('customPlan', JSON.stringify(customPlan));

        this.setState({
            customPlan
        })
    };

    editCustomEntry = (item) => {
        this.setState({
            customPlanItem: item,
            addPlanEntryModalVisible: true
        })
    };

    render() {
        const {Colors} = this.props.screenProps;
        const {loading, updating, date, studyPlan, addPlanEntryModalVisible, customPlan, customPlanItem, tipVisible} = this.state;

        const plan = [
            ...studyPlan,
            ...customPlan
        ];

        return loading
            ? <View style={[Styles.flex, {backgroundColor: Colors.background}]}/>
            : <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
                <WeekPlanHeader Colors={Colors}
                                date={date}
                                left={() => this.setState({date: date - 604800000})}
                                middle={() => this.setState({date: new Date().getTime()})}
                                right={() => this.setState({date: date + 604800000})}
                />
                <WeekPlanBody Colors={Colors}
                              weekPlan={plan}
                              date={date}
                              left={() => this.setState({date: date - 604800000})}
                              right={() => this.setState({date: date + 604800000})}
                              refresh={() => this.setState({updating: true})}
                              loading={updating}
                              removeCustomEntry={this.removeCustomEntry}
                              editCustomEntry={this.editCustomEntry}
                />
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        bottom: 20,
                        right: 20,
                        borderWidth: 1,
                        borderColor: 'rgba(0,0,0,0.2)',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 60,
                        height: 60,
                        backgroundColor: Colors.primaryDark,
                        borderRadius: 50,
                    }}
                    onPress={() => this.setState({
                        addPlanEntryModalVisible: true
                    })}
                >
                    <FontAwesome5 name={"plus"} size={18} color={Colors.onPrimary}/>
                </TouchableOpacity>
                <AddPlanEntryModal customPlan={customPlan}
                                   visible={addPlanEntryModalVisible}
                                   Colors={Colors}
                                   onClose={() => this.setState({
                                       addPlanEntryModalVisible: false,
                                       customPlanItem: {}
                                   })}
                                   onAddItem={this.onAddItem}
                                   onSaveItem={this.onSaveItem}
                                   customPlanItem={customPlanItem}
                />
                <TipWeekModal visible={tipVisible} onClose={() => {
                    this.setState({tipVisible: false})
                }} Colors={Colors}/>
            </View>
    }
}
