import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import * as _ from "lodash";
import {timeLine} from "../../utils";

type Props = {
    Colors: Object,
    plan: Array,
    height: number,
    date: any,
    openPlanDetailModal: Function
}

type State = {}

export default class PlanSmallItem extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        let {Colors, plan, height, date, openPlanDetailModal} = this.props;
        const {} = this.state;

        const dayTime = Math.round(date.setHours(0, 0, 0) / 1000);
        const dayStartTime = Math.round(date.setHours(_.first(timeLine), 0, 0) / 1000);
        const endTime = Math.round(date.setHours(23, 59, 59) / 1000);
        const dayEndTime = Math.round(date.setHours(_.last(timeLine) + 1, 0, 0) / 1000);
        const dayTimeRange = dayEndTime - dayStartTime;

        return _.map(plan, (item, index) => {
            if (dayStartTime <= item.start && item.end <= dayEndTime) {

                const entryArray = _.filter(plan, (el) => (item.start >= el.start && el.start >= item.end) || (item.end >= el.end && el.end >= item.start));
                const entryAmount = entryArray.length;

                const itemHeight = (item.end - item.start) * height / dayTimeRange;
                const top = (item.start - dayStartTime) * height / dayTimeRange;

                const startDate = new Date(item.start * 1000);
                const startHours = startDate.getHours();
                const startMinutes = "0" + startDate.getMinutes();
                const startFormattedTime = startHours + ':' + startMinutes.substr(-2);

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArray)}
                                         style={{
                                             width: '85%',
                                             height: itemHeight,
                                             position: 'absolute',
                                             top: top,
                                             backgroundColor: Colors.background,
                                             shadowOpacity: 10,
                                             elevation: 10
                                         }}
                >
                    <View
                        style={{
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            paddingLeft: 3,
                            backgroundColor: item.color.includes('#') ? item.color : Colors.primaryDark
                        }}
                    >
                        <Text numberOfLines={1}
                              style={{
                                  color: Colors.onPrimary,
                                  fontSize: 9
                              }}
                        >
                            {startFormattedTime}
                        </Text>
                    </View>
                    <View style={{
                        flex: 1,
                        backgroundColor: Colors.background
                    }}>
                        <Text numberOfLines={itemHeight < 55 ? 1 : 2} style={{
                            flexGrow: 1,
                            color: Colors.onBackground,
                            fontSize: 11,
                            paddingLeft: 3,
                        }}>
                            {
                                entryAmount > 1
                                    ? entryAmount + ' Einträge'
                                    : item.title
                            }
                        </Text>
                        {
                            entryAmount > 1
                                ? null
                                : <Text style={{
                                    color: Colors.onBackground,
                                    fontSize: 9,
                                    textAlign: 'right',
                                    paddingRight: 3
                                }}>
                                    {item.room}
                                </Text>
                        }
                    </View>
                </TouchableOpacity>
            } else if (dayTime <= item.start && item.start <= dayStartTime) {
                const entryArrayStart = _.filter(plan, (el) => dayTime <= el.start && el.start <= dayStartTime);
                const entryAmountStart = entryArrayStart.length;

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArrayStart)}
                                         style={{
                                             width: '95%',
                                             position: 'absolute',
                                             top: 0,
                                             backgroundColor: Colors.primaryDark,
                                             shadowOpacity: 10,
                                             elevation: 10,
                                             alignItems: 'center',
                                             justifyContent: 'center'
                                         }}
                >
                    <Text numberOfLines={2} style={{
                        color: Colors.onPrimaryDark,
                        fontSize: 11,
                        padding: 3,
                        textAlign: 'center'
                    }}>
                        {entryAmountStart > 1 ? entryAmountStart + ' Einträge' : entryAmountStart + ' Eintrag'}
                    </Text>
                </TouchableOpacity>
            } else if (dayEndTime <= item.end && item.end <= endTime) {
                const entryArrayEnd = _.filter(plan, (el) => dayEndTime <= el.end && el.end <= endTime);
                const entryAmountEnd = entryArrayEnd.length;

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArrayEnd)}
                                         style={{
                                             width: '95%',
                                             position: 'absolute',
                                             bottom: 0,
                                             backgroundColor: Colors.primaryDark,
                                             shadowOpacity: 10,
                                             elevation: 10,
                                             alignItems: 'center',
                                             justifyContent: 'center'
                                         }}
                >
                    <Text numberOfLines={2} style={{
                        color: Colors.onPrimaryDark,
                        fontSize: 11,
                        padding: 3,
                    }}>
                        {entryAmountEnd > 1 ? entryAmountEnd + ' Einträge' : entryAmountEnd + ' Eintrag'}
                    </Text>
                </TouchableOpacity>
            }
        })
    }
}
