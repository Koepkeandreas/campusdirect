import React from 'react';
import {Keyboard, TouchableWithoutFeedback, View} from 'react-native';

const DismissKeyboard = (Comp) => {
    return ({children, ...props}) => (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <Comp {...props}>
                {children}
            </Comp>
        </TouchableWithoutFeedback>
    );
};
const DismissKeyboardView = DismissKeyboard(View);
export default DismissKeyboardView;
