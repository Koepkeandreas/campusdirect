import React, {Component} from 'react';
import {Dimensions, ScrollView, Text, View} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import Modal from "react-native-modal";
import Form from "../Form";
import Icon from "react-native-vector-icons/FontAwesome5";
import DateTimePicker from '@react-native-community/datetimepicker';

import _ from 'lodash';
import {ColorPicker} from 'react-native-color-picker';
import {Button} from "react-native-elements";
import dayjs from 'dayjs';

let {height} = Dimensions.get('window');

type Props = {
    visible: boolean,
    Colors: Object,
    onClose: Function,
    onAddItem: Function,
    onSaveItem: Function,
    customPlan: Array,
    customPlanItem: Object
}

type State = {}

export default class AddPlanEntryModal extends Component<Props, State> {
    state = {
        start: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.start : new Date().getTime(),
        end: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.end : new Date().getTime() + (60 * 60 * 1000),
        showDatePicker: false,
        showColorPicker: false,
        color: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.color : this.props.Colors.primaryDark,
        minimumDate: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.start : new Date().getTime(),
        title: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.title : '',
        description: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.description : '',
        room: !_.isEmpty(this.props.customPlanItem) ? this.props.customPlanItem.room : '',
    };

    componentDidMount(): void {

    }

    cancel = () => {
        const {onClose} = this.props;

        this.setState({
            start: new Date().getTime(),
            end: new Date().getTime()
        });

        onClose();
    };

    addItem = () => {
        const {customPlan, customPlanItem, onSaveItem, onAddItem} = this.props;
        const {title, description, start, end, room, color} = this.state;

        if (start < end) {
            if (!_.isEmpty(customPlanItem)) {
                onSaveItem({
                    id: customPlanItem.id,
                    custom: true,
                    title,
                    description,
                    start: start / 1000,
                    end: end / 1000,
                    room,
                    color
                });
            } else {
                const i = _.sortBy(customPlan, 'id', 'desc');
                let id = null;
                if (customPlan.length > 0) id = _.first(i).id;

                onAddItem({
                    id: _.isNil(id) ? 0 : id + 1,
                    custom: true,
                    title,
                    description,
                    start: start / 1000,
                    end: end / 1000,
                    room,
                    color
                });
            }
        } else {
            alert('Das Startdatum muss vor dem Enddatum liegen!')
        }
    };

    render() {
        const {visible, Colors} = this.props;
        const {title, description, start, end, date, mode, display, onChange, showDatePicker, room, color, showColorPicker, minimumDate} = this.state;

        const attributes = [
            {
                type: 'textInput',
                placeholder: 'Recherche',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Titel',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (title) => this.setState({title}),
                defaultValue: title,
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                }
            },
            {
                type: 'textInput',
                placeholder: 'Bücher besorgen ...',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Beschreibung',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (description) => this.setState({description}),
                defaultValue: description,
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                }
            },
            {
                type: 'textInput',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Start',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (start) => this.setState({start}),
                defaultValue: dayjs(start).format('DD.MM.YYYY HH:mm'),
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                rightIcon: <Icon
                    name='calendar-alt'
                    size={20}
                    color={Colors.onBackground}
                    onPress={() => {
                        let day = new Date(start).getDate();
                        let month = new Date(start).getMonth();
                        let year = new Date(start).getFullYear();
                        let hour = new Date(start).getHours();
                        let minute = new Date(start).getMinutes();

                        this.setState({
                            date: start,
                            mode: 'date',
                            display: 'calendar',
                            maximumDate: end,
                            minimumDate: null,
                            showDatePicker: true,
                            onChange: (event, selectedDate) => {
                                if (event.type === 'set' && !_.isNil(selectedDate)) {
                                    if (this.state.mode === 'date') {
                                        day = new Date(selectedDate).getDate();
                                        month = new Date(selectedDate).getMonth();
                                        year = new Date(selectedDate).getFullYear();

                                        this.setState({
                                            start: new Date(year, month, day, hour, minute).getTime(),
                                            date: new Date(year, month, day, hour, minute).getTime(),
                                            mode: 'time',
                                            display: 'clock'
                                        })
                                    } else {
                                        hour = new Date(selectedDate).getHours();
                                        minute = new Date(selectedDate).getMinutes();

                                        this.setState({
                                            start: new Date(year, month, day, hour, minute).getTime(),
                                            end: new Date(year, month, day, hour, minute, 59).getTime() >= end ? new Date(year, month, day, hour, minute, 0, 0).getTime() + (1000 * 60 * 60) : end,
                                            showDatePicker: false
                                        })
                                    }
                                } else {
                                    this.setState({
                                        showDatePicker: false
                                    })
                                }
                            }
                        })
                    }}
                />
            },
            {
                type: 'textInput',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Ende',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (end) => this.setState({end}),
                defaultValue: dayjs(end).format('DD.MM.YYYY HH:mm'),
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                },
                rightIcon: <Icon
                    name='calendar-alt'
                    size={20}
                    color={Colors.onBackground}
                    onPress={() => {
                        let day = new Date(end).getDate();
                        let month = new Date(end).getMonth();
                        let year = new Date(end).getFullYear();
                        let hour = new Date(end).getHours();
                        let minute = new Date(end).getMinutes();

                        this.setState({
                            date: end,
                            mode: 'date',
                            display: 'calendar',
                            maximumDate: null,
                            minimumDate: start,
                            showDatePicker: true,
                            onChange: (event, selectedDate) => {
                                if (event.type === 'set' && !_.isNil(selectedDate)) {
                                    if (this.state.mode === 'date') {
                                        day = new Date(selectedDate).getDate();
                                        month = new Date(selectedDate).getMonth();
                                        year = new Date(selectedDate).getFullYear();

                                        this.setState({
                                            end: new Date(year, month, day, hour, minute).getTime(),
                                            date: new Date(year, month, day, hour, minute).getTime(),
                                            mode: 'time',
                                            display: 'clock'
                                        })
                                    } else {
                                        hour = new Date(selectedDate).getHours();
                                        minute = new Date(selectedDate).getMinutes();

                                        this.setState({
                                            end: new Date(year, month, day, hour, minute).getTime(),
                                            showDatePicker: false
                                        })
                                    }
                                } else {
                                    this.setState({
                                        showDatePicker: false
                                    })
                                }
                            }
                        })
                    }}
                />
            },
            {
                type: 'textInput',
                placeholder: 'Bibliothek',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Ort',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (room) => this.setState({room}),
                defaultValue: room,
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: Colors.onBackground,
                    fontSize: 16
                }
            },
            {
                type: 'textInput',
                placeholderTextColor: Colors.onBackgroundTrans,
                label: 'Farbe',
                labelStyle: {
                    color: Colors.onBackgroundTrans,
                    fontSize: 12
                },
                onChange: (color) => this.setState({color}),
                defaultValue: color,
                containerStyle: {
                    width: '80%',
                    minWidth: 250,
                    marginBottom: 20
                },
                inputStyle: {
                    color: color ? color : Colors.onBackground,
                    fontSize: 16
                },
                rightIcon: <Icon
                    name='palette'
                    size={20}
                    color={Colors.onBackground}
                    onPress={() => {
                        this.setState({
                            showColorPicker: true
                        })
                    }}
                />
            }
        ];

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={this.cancel}
            onBackButtonPress={this.cancel}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.background,
                borderRadius: 3,
                shadowOpacity: 15,
                elevation: 15,
                marginBottom: 5,
                marginTop: 5,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 25,
                                    justifyContent: 'space-between',
                                    backgroundColor: Colors.primary,
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderColor: Colors.primary
                                }}
                >
                    <Text style={{
                        color: Colors.onPrimary,
                        fontWeight: 'bold',
                        fontSize: 16,
                        paddingLeft: 10
                    }}>
                        Eigener Eintrag
                    </Text>
                </LinearGradient>
                <View style={{
                    backgroundColor: Colors.background,
                    padding: 10,
                    maxHeight: height * 0.6,
                    alignItems: 'center'
                }}>
                    <ScrollView style={{
                        width: '100%'
                    }}>
                        <Form form={attributes}
                              formStyle={{alignItems: 'center', width: '100%'}}
                        />
                    </ScrollView>
                    {
                        showDatePicker
                            ? <DateTimePicker
                                value={date}
                                mode={mode}
                                display={display}
                                is24Hour={true}
                                onChange={onChange}
                                minimumDate={minimumDate}
                            />
                            : null
                    }
                    <Modal
                        backdropOpacity={0.3}
                        isVisible={showColorPicker}
                        disableAnimation={true}
                        onBackdropPress={() => this.setState({
                            showColorPicker: false
                        })}
                        onBackButtonPress={() => this.setState({
                            showColorPicker: false
                        })}
                        style={{alignItems: 'center'}}
                    >
                        <View style={{
                            width: '90%',
                            backgroundColor: Colors.background,
                            borderRadius: 3,
                            shadowOpacity: 15,
                            elevation: 15,
                            padding: 15
                        }}>
                            <View style={{
                                width: '100%',
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: Colors.onBackground,
                                    textAlign: 'center'
                                }}>
                                    Drücke auf den mittleren Kreis, um die Farbe zu übernehmen.
                                </Text>
                            </View>
                            <ColorPicker
                                onColorSelected={color => {
                                    this.setState({
                                        color: color,
                                        showColorPicker: false
                                    })
                                }}
                                style={{
                                    width: '100%',
                                    height: 200,
                                }}
                                defaultColor={color}
                            />
                        </View>
                    </Modal>
                </View>
                <View style={{
                    backgroundColor: Colors.background,
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'center'
                }}>
                    <Button raised={true}
                            title={'Abbrechen'}
                            onPress={() => {
                                this.cancel();
                            }}
                            titleStyle={{
                                color: Colors.onPrimary
                            }}
                            containerStyle={{
                                width: '50%'
                            }}
                            buttonStyle={{
                                borderRadius: 0,
                                height: 35
                            }}
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: [Colors.primaryDark, Colors.primary],
                                start: {x: 0, y: 1},
                                end: {x: 0, y: 0},
                            }}
                    />
                    <Button raised={true}
                            title={'Speichern'}
                            onPress={this.addItem}
                            titleStyle={{
                                color: Colors.onPrimary
                            }}
                            containerStyle={{
                                width: '50%'
                            }}
                            buttonStyle={{
                                borderRadius: 0,
                                height: 35
                            }}
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: [Colors.primaryDark, Colors.primary],
                                start: {x: 0, y: 1},
                                end: {x: 0, y: 0},
                            }}
                    />
                </View>
            </View>
        </Modal>
    }
}
