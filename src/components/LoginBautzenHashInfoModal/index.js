import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import Modal from "react-native-modal";

import {_goToURL} from "../../utils";
import Picture from "../Picture";
import LinearGradient from "react-native-linear-gradient";

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object
}

export default class LoginBautzenHashInfoModal extends Component<Props> {
    render() {
        const {visible, onClose, Colors} = this.props;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.PlanBackground,
                borderRadius: 3,
                shadowOpacity: 5,
                shadowRadius: 15,
                elevation: 15,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 30,
                                    justifyContent: 'center',
                                    backgroundColor: Colors.primary
                                }}
                >
                    <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: Colors.onPrimary,
                        paddingLeft: 10
                    }}>
                        Bautzen Hash
                    </Text>
                </LinearGradient>
                <View style={{
                    padding: 5
                }}>
                    <ScrollView>
                        <View style={{
                            width: '100%',
                            alignItems: 'center',
                            padding: 0,
                            justifyContent: 'center'
                        }}>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={{color: Colors.onBackground}}>1. {'Log dich ein auf: '}</Text>
                                <Text style={{color: Colors.primary, textDecorationLine: 'underline'}}
                                      onPress={() => _goToURL('https://intranet.ba-bautzen.de/login/')}>BA
                                    Bautzen</Text>
                            </View>
                            <Text style={{
                                paddingHorizontal: 10,
                                paddingVertical: 10,
                                textAlign: 'center',
                                color: Colors.onBackground
                            }}>2. {'Geh zu den Links und kopiere den Android-Hash oder scanne den QR-Code ein.'}</Text>
                            <Picture type={'bautzenHash'}
                                     imgWidth={280}
                                     style={{paddingTop: 10, paddingBottom: 10}}
                                     Colors={Colors}
                            />
                        </View>
                    </ScrollView>
                </View>
            </View>
        </Modal>
    }
}

