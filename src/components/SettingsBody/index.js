import React, {Component} from 'react';
import {Alert, Dimensions, View} from 'react-native';
import {Styles} from "../../utils/styles";
import {Button, CheckBox} from "react-native-elements";
import FilterModal from '../FilterModal'
import LinearGradient from "react-native-linear-gradient";
import _ from 'lodash';
import AsyncStorage from "@react-native-community/async-storage";
import CampusDualLinkModal from "../CampusDualLinkModal";

const WIDTH = Dimensions.get('window').width;

type Props = {
    Colors: Object,
    useDarkTheme: boolean,
    reload: Function,
    changeTheme: Function,
    campusIndex: string,
    studyNumber: string,
    campusDualHash: string,
    ts: number,
    showTips: boolean
}

type State = {
    loading: boolean,
    items: Array,
    selectedItems: Array,
    visible: boolean,
    campusDualLoginModalVisible: boolean,
    showTips: boolean
}

export default class SettingsBody extends Component<Props, State> {
    state = {
        loading: true,
        items: [],
        selectedItems: [],
        visible: false,
        campusDualLoginModalVisible: false,
        showTips: this.props.showTips
    };

    async componentDidMount(): void {
        if (this.state.loading) {
            let studyPlanModuleList = await AsyncStorage.getItem('studyPlanModuleList');
            if (studyPlanModuleList !== null) {
                try {
                    studyPlanModuleList = JSON.parse(studyPlanModuleList);
                } catch (e) {
                    studyPlanModuleList = [];
                }
            }

            let studyPlanFilter = await AsyncStorage.getItem('studyPlanFilter');
            if (studyPlanFilter !== null) {
                try {
                    studyPlanFilter = JSON.parse(studyPlanFilter);
                } catch (e) {
                    studyPlanFilter = [];
                }
            }

            this.setState({
                loading: false,
                items: studyPlanModuleList,
                selectedItems: studyPlanFilter
            })
        }
    }

    onFilterChange = async (selectedItems) => {
        if (JSON.stringify(selectedItems) !== JSON.stringify(this.state.selectedItems)) {
            const studyPlanFilter = _.map(selectedItems, (item: Object) => item.value);

            await AsyncStorage.setItem('studyPlanFilter', JSON.stringify(studyPlanFilter));
            this.setState({selectedItems: studyPlanFilter, visible: false});
            this.props.reload();
        } else {
            this.setState({
                visible: false
            })
        }
    };

    removeCampusDualLink = async () => {
        await AsyncStorage.removeItem('studyNumber');
        await AsyncStorage.removeItem('campusDualHash');
        await AsyncStorage.removeItem('encryptedCampusDualHash');
        await AsyncStorage.removeItem('encryptedStudyNumber');
        this.props.reload();
    };

    render() {
        const {Colors, useDarkTheme, changeTheme, campusDualHash, studyNumber, campusIndex, reload, ts} = this.props;
        const {selectedItems, visible, items, loading, campusDualLoginModalVisible, showTips} = this.state;

        return <View style={[Styles.flex, Styles.center]}>
            <CheckBox
                title='Dunkler Modus'
                checked={useDarkTheme}
                onPress={async () => {
                    await AsyncStorage.setItem('useDarkTheme', String(!useDarkTheme));
                    changeTheme();
                }}
                containerStyle={{
                    padding: 5,
                    margin: 0,
                    borderWidth: 0,
                    backgroundColor: Colors.background
                }}
                iconRight
                checkedColor={Colors.primary}
            />
            <CheckBox
                title='Tipps anzeigen'
                checked={showTips}
                onPress={async () => {
                    await AsyncStorage.setItem('showTips', String(!showTips));
                    this.setState({
                        showTips: !showTips
                    });
                    reload();
                }}
                containerStyle={{
                    padding: 5,
                    margin: 0,
                    borderWidth: 0,
                    backgroundColor: Colors.background
                }}
                iconRight
                checkedColor={Colors.primary}
            />
            {
                loading || items === null || items.length === 0
                    ? null
                    : <Button raised={true}
                              title={'Kalender Filter'}
                              onPress={() => {
                                  if (items) {
                                      this.setState({visible: true})
                                  } else {
                                      Alert.alert(
                                          'Neustart',
                                          'Du musst die App kurz neustarten, damit du das benutzen kannst!'
                                      )
                                  }
                              }}
                              titleStyle={{
                                  color: Colors.onPrimary
                              }}
                              containerStyle={{
                                  margin: 5
                              }}
                              buttonStyle={{
                                  width: WIDTH * 0.7,
                                  height: 35
                              }}
                              ViewComponent={LinearGradient}
                              linearGradientProps={{
                                  colors: [Colors.primaryDark, Colors.primary],
                                  start: {x: 1, y: 1},
                                  end: {x: 0, y: 0},
                              }}
                    />
            }
            {
                campusIndex !== 'ba_bz'
                    ? null
                    : <Button raised={true}
                              title={_.isNil(campusDualHash) ? 'CampusDual verknüpfen' : 'CampusDual entfernen'}
                              onPress={() => {
                                  if (_.isNil(campusDualHash)) {
                                      this.setState({
                                          campusDualLoginModalVisible: true
                                      })
                                  } else {
                                      Alert.alert(
                                          'Verknüfung löschen',
                                          'Möchtest du die Verknüfung zu CampusDual wirklich löschen?',
                                          [
                                              {
                                                  text: 'Nein'
                                              },
                                              {text: 'Löschen', onPress: () => this.removeCampusDualLink()},
                                          ],
                                      )
                                  }
                              }}
                              titleStyle={{
                                  color: Colors.onPrimary
                              }}
                              containerStyle={{
                                  margin: 5
                              }}
                              buttonStyle={{
                                  width: WIDTH * 0.7,
                                  height: 35
                              }}
                              ViewComponent={LinearGradient}
                              linearGradientProps={{
                                  colors: [Colors.primaryDark, Colors.primary],
                                  start: {x: 1, y: 1},
                                  end: {x: 0, y: 0},
                              }}
                    />
            }
            {
                items && items.length > 0
                    ? <FilterModal
                        calendarFilterList={items}
                        selectedItems={selectedItems}
                        visible={visible}
                        onClose={() => this.setState({
                            visible: false
                        })}
                        onFilterChange={this.onFilterChange}
                        Colors={Colors}
                    />
                    : null
            }
            {
                campusIndex !== 'ba_bz'
                    ? null
                    : <CampusDualLinkModal visible={campusDualLoginModalVisible}
                                           onClose={() => this.setState({campusDualLoginModalVisible: false})}
                                           Colors={Colors}
                                           reload={reload}
                                           ts={ts}
                    />
            }
        </View>
    }
}
