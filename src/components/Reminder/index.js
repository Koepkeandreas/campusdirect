import React, {Component} from 'react';
import {ActivityIndicator, Alert, Linking, View} from 'react-native';
import {Badge, Button} from 'react-native-elements';
import {_goToURL, fetchReminder} from "../../utils";
import LinearGradient from "react-native-linear-gradient";
import ReminderUpcomingModal from "../ReminderUpcomingModal";
import ReminderLatestModal from "../ReminderLatestModal";
import AsyncStorage from "@react-native-community/async-storage";
import _ from "lodash";

type Props = {
    navigation: any,
    Colors: Object,
    studyNumber: string,
    campusDualHash: string
}

type State = {
    loading: boolean,
    updating: boolean,
    reminder: Object,
    newExam: boolean,
    newElectives: boolean,
    newUpcoming: boolean,
    newLatest: boolean,
    upcomingModalVisible: boolean,
    latestModalVisible: boolean,
    nextRateDate: Date,
    rated: boolean
}

export default class Reminder extends Component<Props, State> {
    state = {
        loading: true,
        updating: true,
        reminder: {},
        newExam: false,
        newElectives: false,
        newUpcoming: false,
        newLatest: false,
        upcomingModalVisible: false,
        latestModalVisible: false,
        rated: false
    };

    async componentDidMount(): void {
        const {campusDualHash, studyNumber} = this.props;

        if (this.state.loading) {
            let reminder = await AsyncStorage.getItem('reminder');
            if (reminder !== null) {
                try {
                    reminder = JSON.parse(reminder);
                } catch (e) {
                    reminder = {};
                }
            } else reminder = {};

            let rated = await AsyncStorage.getItem('rated');
            if (rated !== null) rated = rated === 'true';
            else rated = false;

            let nextRateDate = await AsyncStorage.getItem('nextRateDate');
            if (nextRateDate !== null) nextRateDate = parseInt(nextRateDate);
            else {
                nextRateDate = new Date().getTime() + 1000 * 60 * 60 * 24 * 3;
                await AsyncStorage.setItem('nextRateDate', String(nextRateDate));
            }

            this.setState({
                loading: false,
                reminder,
                nextRateDate,
                rated
            });

            if (this.state.updating && campusDualHash !== null) {
                setTimeout(() => {
                    fetchReminder(studyNumber, campusDualHash, async (data: Object) => {
                        try {
                            const newReminder = JSON.parse(data);
                            await AsyncStorage.setItem('reminder', JSON.stringify(newReminder));


                            this.setState({
                                updating: false,
                                reminder: newReminder
                            })
                        } catch (e) {
                            alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                        }
                    }, () => {
                        this.setState({
                            updating: false
                        })
                    })
                }, 100)
            }
        }
    }

    render() {
        const {Colors, campusDualHash} = this.props;
        const {loading, reminder, newElectives, newExam, newLatest, newUpcoming, latestModalVisible, upcomingModalVisible, nextRateDate, rated, updating} = this.state;

        let EXAMS, ELECTIVES, UPCOMING, LATEST = null;
        if (reminder) {
            EXAMS = reminder.EXAMS;
            ELECTIVES = reminder.ELECTIVES;
            UPCOMING = reminder.UPCOMING;
            LATEST = reminder.LATEST;
        }

        if (!rated && new Date().getTime() >= nextRateDate) {
            Alert.alert(
                'CampusDirect bewerten',
                'Gefällt dir die App? Teile deine Erfahrungen mit allen und Bewerte CampusDirect!',
                [
                    {
                        text: 'Später', onPress: async () => {
                            const nextRateDate = new Date().getTime() + 1000 * 60 * 60 * 24 * 2;
                            await AsyncStorage.setItem('nextRateDate', String(nextRateDate));
                            this.setState({
                                nextRateDate
                            });
                        }
                    },
                    {
                        text: 'Nein', onPress: async () => {
                            await AsyncStorage.setItem('rated', 'true');
                            this.setState({
                                nextRateDate: 0,
                                rated: true
                            });
                        }
                    },
                    {
                        text: 'Ja', onPress: async () => {
                            await AsyncStorage.setItem('rated', 'true');
                            this.setState({
                                nextRateDate: 0,
                                rated: true
                            });
                            Linking.openURL("http://play.google.com/store/apps/details?id=com.campusdirect");
                        }
                    },
                ],
            )
        }

        return <View style={{width: '100%', backgroundColor: Colors.background}}>
            {
                _.isNil(campusDualHash)
                    ? <View style={{width: '100%', paddingHorizontal: 10, paddingBottom: 10}}>
                        <Button raised={true}
                                title={'Verknüpfen Sie Ihr CampusDual Profil, um Termine, Noten und Anmeldungen hier zu sehen.'}
                                onPress={() => {
                                    this.props.navigation.navigate('Settings')
                                }}
                                titleStyle={{
                                    color: Colors.onPrimary,
                                    marginVertical: 2
                                }}
                                ViewComponent={LinearGradient}
                                linearGradientProps={{
                                    colors: [Colors.primaryDark, Colors.primary],
                                    start: {x: 1, y: 1},
                                    end: {x: 0, y: 0},
                                }}
                        />
                    </View>
                    : <View style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        flexWrap: 'wrap'
                    }}>
                        {
                            !_.isNil(EXAMS) && EXAMS > 0
                                ? <View>
                                    <Button raised={true}
                                            title={'Anmeldung'}
                                            onPress={() => {
                                                _goToURL('https://erp.campus-dual.de/sap/bc/webdynpro/sap/zba_initss?sap-client=100&sap-language=de&uri=https://selfservice.campus-dual.de/index/login');
                                            }}
                                            titleStyle={{
                                                color: Colors.onPrimary,
                                                fontSize: 14
                                            }}
                                            containerStyle={{
                                                marginBottom: 5
                                            }}
                                            ViewComponent={LinearGradient}
                                            linearGradientProps={{
                                                colors: [Colors.primaryDark, Colors.primary],
                                                start: {x: 1, y: 1},
                                                end: {x: 0, y: 0},
                                            }}
                                    />
                                    <Badge
                                        containerStyle={{
                                            position: 'absolute',
                                            top: -5,
                                            right: -5,
                                            shadowOpacity: 10,
                                            elevation: 10
                                        }}
                                        value={EXAMS}
                                        badgeStyle={{
                                            backgroundColor: newExam ? Colors.error : Colors.primaryDark
                                        }}
                                    />
                                </View>
                                : null
                        }
                        {
                            !_.isNil(ELECTIVES) && ELECTIVES > 0
                                ? <View style={{marginBottom: 5}}>
                                    <Button raised={true}
                                            title={'Wählen'}
                                            onPress={() => {
                                                _goToURL('https://erp.campus-dual.de/sap/bc/webdynpro/sap/zba_initss?sap-client=100&sap-language=de&uri=https://selfservice.campus-dual.de/index/login');
                                            }}
                                            titleStyle={{
                                                color: Colors.onPrimary,
                                                fontSize: 14
                                            }}
                                            ViewComponent={LinearGradient}
                                            linearGradientProps={{
                                                colors: [Colors.primaryDark, Colors.primary],
                                                start: {x: 1, y: 1},
                                                end: {x: 0, y: 0},
                                            }}
                                    />
                                    <Badge
                                        containerStyle={{
                                            position: 'absolute',
                                            top: -5,
                                            right: -5,
                                            shadowOpacity: 10,
                                            elevation: 10
                                        }}
                                        value={ELECTIVES}
                                        badgeStyle={{
                                            backgroundColor: newElectives ? Colors.error : Colors.primaryDark
                                        }}
                                    />
                                </View>
                                : null
                        }
                        {
                            !_.isNil(UPCOMING) && UPCOMING.length > 0
                                ? <View>
                                    <Button raised={true}
                                            title={'Termine'}
                                            onPress={() => {
                                                this.setState({upcomingModalVisible: true});
                                            }}
                                            titleStyle={{
                                                color: Colors.onPrimary,
                                                fontSize: 14
                                            }}
                                            containerStyle={{
                                                marginBottom: 5
                                            }}
                                            ViewComponent={LinearGradient}
                                            linearGradientProps={{
                                                colors: [Colors.primaryDark, Colors.primary],
                                                start: {x: 1, y: 1},
                                                end: {x: 0, y: 0},
                                            }}
                                    />
                                    <Badge
                                        containerStyle={{
                                            position: 'absolute',
                                            top: -5,
                                            right: -5,
                                            shadowOpacity: 10,
                                            elevation: 10
                                        }}
                                        value={UPCOMING.length}
                                        badgeStyle={{
                                            backgroundColor: newUpcoming ? Colors.error : Colors.primaryDark
                                        }}
                                    />
                                </View>
                                : null
                        }
                        {
                            !_.isNil(LATEST) && LATEST.length > 0
                                ? <View>
                                    <Button raised={true}
                                            title={'Noten'}
                                            onPress={() => {
                                                this.setState({latestModalVisible: true});
                                            }}
                                            titleStyle={{
                                                color: Colors.onPrimary,
                                                fontSize: 14
                                            }}
                                            containerStyle={{
                                                marginBottom: 5
                                            }}
                                            ViewComponent={LinearGradient}
                                            linearGradientProps={{
                                                colors: [Colors.primaryDark, Colors.primary],
                                                start: {x: 1, y: 1},
                                                end: {x: 0, y: 0},
                                            }}
                                    />
                                    <Badge
                                        containerStyle={{
                                            position: 'absolute',
                                            top: -5,
                                            right: -5,
                                            shadowOpacity: 10,
                                            elevation: 10
                                        }}
                                        value={LATEST.length}
                                        badgeStyle={{
                                            backgroundColor: newLatest ? Colors.error : Colors.primaryDark
                                        }}
                                    />
                                </View>
                                : null
                        }
                        <ReminderUpcomingModal visible={upcomingModalVisible}
                                               onClose={() => this.setState({upcomingModalVisible: false})}
                                               data={UPCOMING}
                                               Colors={Colors}
                        />
                        <ReminderLatestModal visible={latestModalVisible}
                                             onClose={() => this.setState({latestModalVisible: false})}
                                             data={LATEST}
                                             Colors={Colors}
                        />
                    </View>
            }
            {
                updating
                    ? <View style={{
                        position: 'absolute',
                        right: 10,
                        bottom: 30,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size="small" color={Colors.primary}/>
                    </View>
                    : null
            }
        </View>
    }
}
