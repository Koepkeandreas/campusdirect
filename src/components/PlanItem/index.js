import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import * as _ from "lodash";
import {timeLine} from "../../utils";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {Styles} from "../../utils/styles";

type Props = {
    Colors: Object,
    plan: Array,
    height: number,
    date: any,
    openPlanDetailModal: Function
}

type State = {}

export default class PlanItem extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, plan, height, date, openPlanDetailModal} = this.props;
        const {} = this.state;

        const dayTime = Math.round(date.setHours(0, 0, 0) / 1000);
        const dayStartTime = Math.round(date.setHours(_.first(timeLine), 0, 0) / 1000);
        const endTime = Math.round(date.setHours(23, 59, 59) / 1000);
        const dayEndTime = Math.round(date.setHours(_.last(timeLine) + 1, 0, 0) / 1000);
        const dayTimeRange = dayEndTime - dayStartTime;

        const styles = {
            title: {
                flex: 1,
                color: Colors.onPrimaryDark,
                fontSize: 14
            },
            room: {
                color: Colors.onPrimaryDark,
                fontSize: 12
            },
            instructor: {
                flexDirection: 'row',
                alignItems: 'center',
                paddingRight: 10
            },
            text: {
                color: Colors.onBackground,
                fontSize: 12,
                marginLeft: 5
            }
        };

        return _.map(plan, (item, index) => {
            if (dayStartTime <= item.start && item.end <= dayEndTime) {

                const entryArray = _.filter(plan, {"start": item.start});
                const entryAmount = entryArray.length;

                const itemHeight = (item.end - item.start) * height / dayTimeRange;
                const top = (item.start - dayStartTime) * height / dayTimeRange;

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArray)}
                                         style={{
                                             width: '90%',
                                             height: itemHeight,
                                             position: 'absolute',
                                             top: top,
                                             backgroundColor: Colors.background,
                                             shadowOpacity: 5,
                                             elevation: 5
                                         }}
                >
                    <View
                        style={{
                            height: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            paddingHorizontal: 5,
                            backgroundColor: item.color.includes('#') ? item.color : Colors.primaryDark
                        }}
                    >
                        {
                            entryAmount > 1
                                ? <Text numberOfLines={1} style={styles.title}>
                                    {entryAmount} {'Einträge'}
                                </Text>
                                : <Text numberOfLines={1} style={styles.title}>
                                    {item.title}
                                </Text>
                        }
                        <Text style={styles.room}>
                            {
                                entryAmount > 1
                                    ? null
                                    : item.room
                            }
                        </Text>
                    </View>
                    <View style={{
                        flex: 1,
                        paddingLeft: 5,
                        backgroundColor: Colors.background
                    }}>
                        {
                            entryAmount > 1
                                ? <View style={[Styles.flex, styles.instructor]}>
                                    <FontAwesome5 name={'info-circle'}
                                                  size={11}
                                                  color={Colors.onBackground}/>
                                    <Text numberOfLines={1} style={[styles.text, {width: '90%'}]}>
                                        {'Klick für mehr'}
                                    </Text>
                                </View>
                                : <View style={{flex: 1}}>
                                    {
                                        itemHeight >= 39 && !_.isEmpty(item.instructor)
                                            ? <View style={styles.instructor}>
                                                <FontAwesome5 name={'user-tie'} size={11} color={Colors.onBackground}/>
                                                <Text numberOfLines={1} style={[styles.text]}>
                                                    {item.instructor}
                                                </Text>
                                            </View>
                                            : null
                                    }
                                    {
                                        itemHeight >= 54 && !_.isEmpty(item.description)
                                            ? <View style={styles.instructor}>
                                                <FontAwesome5 name={'info-circle'} size={11} color={Colors.onBackground}/>
                                                <Text numberOfLines={1} style={[styles.text]}>
                                                    {item.description}
                                                </Text>
                                            </View>
                                            : null
                                    }
                                    {
                                        itemHeight >= 68 && !_.isEmpty(item.remarks)
                                            ? <Text numberOfLines={1} style={[styles.text]}>
                                                {item.remarks}
                                            </Text>
                                            : null
                                    }
                                </View>
                        }
                    </View>
                </TouchableOpacity>
            } else if (dayTime <= item.start && item.start <= dayStartTime) {
                const entryArrayStart = _.filter(plan, (el) => dayTime <= el.start && el.start <= dayStartTime);
                const entryAmountStart = entryArrayStart.length;

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArrayStart)}
                                         style={{
                                             width: '95%',
                                             position: 'absolute',
                                             top: 0,
                                             backgroundColor: Colors.primaryDark,
                                             shadowOpacity: 10,
                                             elevation: 10,
                                             alignItems: 'center',
                                             justifyContent: 'center'
                                         }}
                >
                    <Text numberOfLines={2} style={{
                        color: Colors.onPrimaryDark,
                        fontSize: 16,
                        padding: 3,
                        textAlign: 'center'
                    }}>
                        {entryAmountStart > 1 ? entryAmountStart + ' Einträge' : entryAmountStart + ' Eintrag'}
                    </Text>
                </TouchableOpacity>
            } else if (dayEndTime <= item.end && item.end <= endTime) {
                const entryArrayEnd = _.filter(plan, (el) => dayEndTime <= el.end && el.end <= endTime);
                const entryAmountEnd = entryArrayEnd.length;

                return <TouchableOpacity key={index}
                                         onPress={() => openPlanDetailModal(entryArrayEnd)}
                                         style={{
                                             width: '95%',
                                             position: 'absolute',
                                             bottom: 0,
                                             backgroundColor: Colors.primaryDark,
                                             shadowOpacity: 10,
                                             elevation: 10,
                                             alignItems: 'center',
                                             justifyContent: 'center'
                                         }}
                >
                    <Text numberOfLines={2} style={{
                        color: Colors.onPrimaryDark,
                        fontSize: 16,
                        padding: 3,
                    }}>
                        {entryAmountEnd > 1 ? entryAmountEnd + ' Einträge' : entryAmountEnd + ' Eintrag'}
                    </Text>
                </TouchableOpacity>
            }
        });
    }
}
