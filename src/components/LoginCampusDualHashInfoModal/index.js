import React, {Component} from 'react';
import {Clipboard, Dimensions, ScrollView, Text, ToastAndroid, View} from 'react-native';
import Modal from "react-native-modal";

import {_goToURL} from "../../utils";
import Picture from "../Picture";
import LinearGradient from "react-native-linear-gradient";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

let {width} = Dimensions.get('window');

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object
};

export default class LoginCampusDualHashInfoModal extends Component<Props> {

    render() {
        const {visible, onClose, Colors} = this.props;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.PlanBackground,
                borderRadius: 3,
                shadowOpacity: 5,
                shadowRadius: 15,
                elevation: 15,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 30,
                                    justifyContent: 'center',
                                    backgroundColor: Colors.primary
                                }}
                >
                    <Text style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        color: Colors.onPrimary,
                        paddingLeft: 10
                    }}>
                        CampusDual Hash
                    </Text>
                </LinearGradient>
                <View style={{
                    padding: 5
                }}>
                    <ScrollView>
                        <View style={{
                            width: '100%',
                            alignItems: 'center',
                            padding: 0,
                            justifyContent: 'center'
                        }}>
                            <View style={{flexDirection: 'row', alignItems: 'center', paddingVertical: 3}}>
                                <Text style={{color: Colors.onBackground}}>1. {'Log dich ein auf: '}</Text>
                                <FontAwesome5Icon name={'external-link-alt'}
                                                  size={10}
                                                  color={Colors.primary}/>
                                <Text style={{color: Colors.primary, textDecorationLine: 'underline'}}
                                      onPress={() => _goToURL('https://erp.campus-dual.de/sap/bc/webdynpro/sap/zba_initss?sap-client=100&sap-language=de&uri=https://selfservice.campus-dual.de/index/login')}>CampusDual</Text>
                            </View>
                            <Text style={{
                                color: Colors.onBackground,
                                textAlign: 'center',
                                paddingTop: 3
                            }}>2. {'Kopiere den fett geschriebenen Link indem du drauf tippst: '}</Text>
                            <Text style={{
                                color: Colors.onBackground,
                                fontWeight: 'bold',
                                textDecorationLine: 'underline',
                                paddingBottom: 3
                            }}
                                  onPress={() => {
                                      ToastAndroid.show('Link wurde kopiert!', ToastAndroid.SHORT);
                                      Clipboard.setString('view-source:https://selfservice.campus-dual.de/index/login')
                                  }}
                            >Link</Text>

                            <Text style={{
                                color: Colors.onBackground,
                                textAlign: 'center',
                                paddingVertical: 3
                            }}>3. {'Geh wieder in dein Browser und füge den Text in die Adresszeile ein.'}</Text>

                            <View style={{flexDirection: 'row', paddingVertical: 3}}>
                                <Text style={{color: Colors.onBackground}}>4. {'Suche nun den Hash auf Zeile: '}</Text>
                                <Text style={{fontWeight: 'bold', color: Colors.onBackground}}>134</Text>
                            </View>
                            <Picture type={'mobileHash'}
                                     imgWidth={width * 0.7}
                                     style={{paddingTop: 10, paddingBottom: 10}}
                                     Colors={Colors}
                            />
                        </View>
                    </ScrollView>
                </View>
            </View>
        </Modal>
    }
}
