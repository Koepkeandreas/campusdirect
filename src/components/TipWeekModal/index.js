import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import Modal from "react-native-modal";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

let {width, height} = Dimensions.get('window');

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object,
}

type State = {}

export default class TipWeekModal extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {visible, onClose, Colors} = this.props;
        const {} = this.state;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
        >
            <View style={{
                position: 'absolute',
                top: 20,
                width: '30%'
            }}>
                <FontAwesome5 name={'arrow-up'}
                              size={35}
                              style={{
                                  paddingBottom: 5
                              }}
                              color={Colors.onBackground}/>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Die Woche kannst du mit den Pfeilen ändern.
                </Text>
            </View>
            <View style={{
                position: 'absolute',
                top: 20,
                left: width * 0.4,
                width: '50%'
            }}>
                <FontAwesome5 name={'arrow-up'}
                              size={35}
                              style={{
                                  paddingBottom: 5
                              }}
                              color={Colors.onBackground}/>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Klicke hier, wenn du zur aktuellen Woche springen möchtest.
                </Text>
            </View>
            <View style={{
                position: 'absolute',
                top: height * 0.4,
                left: width * 0.05,
                width: '90%',
                alignItems: 'center'
            }}>
                <View style={{
                    width: 150,
                    paddingBottom: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <FontAwesome5 name={'arrow-left'}
                                  size={35}
                                  color={Colors.onBackground}/>
                    <FontAwesome5 name={'hand-point-up'}
                                  size={35}
                                  color={Colors.onBackground}/>
                    <FontAwesome5 name={'arrow-right'}
                                  size={35}
                                  color={Colors.onBackground}/>
                </View>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Slide nach Rachts oder nach Links um die Woche zu ändern.
                    Diese Funktion gibt es auch auf der Mensa Seite.
                </Text>
            </View>
            <View style={{
                position: 'absolute',
                bottom: 60,
                flexDirection: 'row',
                width: '70%',
                alignItems: 'center'
            }}>
                <Text style={{
                    color: Colors.onBackground,
                    textAlign: 'center',
                    borderWidth: 1,
                    backgroundColor: Colors.background,
                    borderColor: Colors.border,
                    padding: 5
                }}>
                    Auf dem Plus kannst du einen eigenen Eintrag hinzufügen.
                </Text>
                <FontAwesome5 name={'arrow-right'}
                              size={35}
                              style={{
                                  paddingLeft: 10
                              }}
                              color={Colors.onBackground}/>
            </View>
        </Modal>
    }
}
