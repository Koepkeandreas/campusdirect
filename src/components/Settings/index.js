import React, {Component} from 'react';
import {View} from 'react-native';
import {Styles} from "../../utils/styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import SettingsFooter from "../SettingsFooter";
import SettingsBody from "../SettingsBody";

type Props = {
    screenProps: {
        Colors: Object,
        restart: Function,
        reload: Function,
        useDarkTheme: Boolean,
        changeTheme: Function,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string,
        ts: number,
        showTips: boolean
    }
}

type State = {}

export default class Settings extends Component<Props, State> {
    state = {};

    onLogout = async () => {
        AsyncStorage.clear();
        this.props.screenProps.restart();
    };

    render() {
        const {Colors, useDarkTheme, reload, changeTheme, studyNumber, campusIndex, restart, campusDualHash, showTips, ts} = this.props.screenProps;
        const {} = this.state;

        return <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
            <SettingsBody
                Colors={Colors}
                useDarkTheme={useDarkTheme}
                reload={reload}
                changeTheme={changeTheme}
                studyNumber={studyNumber}
                campusIndex={campusIndex}
                campusDualHash={campusDualHash}
                ts={ts}
                showTips={showTips}
            />
            <SettingsFooter
                Colors={Colors}
                onLogout={() => this.onLogout()}
            />
        </View>
    }
}
