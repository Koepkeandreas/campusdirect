import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import Modal from "react-native-modal";
import LinearGradient from "react-native-linear-gradient";

type Props = {
    visible: boolean,
    onClose: Function,
    Colors: Object,
    campusIndex: string
}

type State = {}

export default class UpdateNewsModal extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {visible, onClose, Colors, campusIndex} = this.props;
        const {} = this.state;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.background,
                borderRadius: 3,
                shadowOpacity: 15,
                elevation: 15,
                marginBottom: 5,
                marginTop: 5,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 25,
                                    justifyContent: 'space-between',
                                    backgroundColor: Colors.primary,
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderColor: Colors.primary
                                }}
                >
                    <Text style={{
                        color: Colors.onPrimary,
                        fontWeight: 'bold',
                        fontSize: 16,
                        paddingLeft: 10
                    }}>
                        Update News
                    </Text>
                </LinearGradient>
                <View style={{
                    backgroundColor: Colors.background,
                    padding: 10
                }}>
                    <ScrollView>
                        <View style={{
                            width: '100%',
                            padding: 0,
                            justifyContent: 'center',
                            maxHeight: 350
                        }}>
                            <Text style={{color: Colors.onBackground}}>- Update- und Ladeverbesserungen</Text>
                            <Text style={{color: Colors.onBackground}}>- Einfügen eines eigenen Termins in den Plan
                                möglich</Text>
                            <Text style={{color: Colors.onBackground}}>- Tipps auf dem Home- und Wochenkalender</Text>
                            <Text style={{color: Colors.onBackground}}>- Filtereinstellungen für den Stundenplan</Text>
                            <Text style={{color: Colors.onBackground}}>- Sicherheitsupdate</Text>
                            <Text style={{color: Colors.onBackground}}>- Verkleinerung der App-Größe</Text>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </Modal>
    }
}
