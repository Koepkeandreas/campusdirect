import React, {Component} from 'react';
import {Alert, ScrollView, Text, View} from 'react-native';
import Modal from "react-native-modal";
import * as _ from "lodash";
import FontAwesome5Icon from "react-native-vector-icons/FontAwesome5";

type Props = {
    Colors: Object,
    visible: boolean,
    onClose: Function,
    items: Array,
    removeCustomEntry?: Function,
    editCustomEntry?: Function
}

type State = {}

export default class PlanDetailModal extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, visible, onClose, items, removeCustomEntry, editCustomEntry} = this.props;
        const {} = this.state;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => onClose()}
            onBackButtonPress={() => onClose()}
            style={{alignItems: 'center'}}
        >
            <ScrollView style={{
                width: '90%',
                maxHeight: items.length > 2 ? '70%' : items.length > 1 ? '45%' : '25%'
            }}
                        contentContainerStyle={{
                            flexGrow: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
            >
                {
                    _.map(items, (item, index) => {

                        const startDate = new Date(item.start * 1000);
                        const startHours = startDate.getHours();
                        const startMinutes = "0" + startDate.getMinutes();
                        const startFormattedTime = startHours + ':' + startMinutes.substr(-2);

                        const endDate = new Date(item.end * 1000);
                        const endHours = endDate.getHours();
                        const endMinutes = "0" + endDate.getMinutes();
                        const endFormattedTime = endHours + ':' + endMinutes.substr(-2);

                        return <View key={index} style={{
                            width: '100%',
                            borderWidth: 1,
                            borderColor: Colors.border,
                            marginBottom: 3
                        }}>
                            <View
                                style={{
                                    height: 25,
                                    justifyContent: 'space-between',
                                    flexDirection: 'row',
                                    shadowOpacity: 15,
                                    elevation: 15,
                                    borderWidth: 1,
                                    borderColor: item.color.includes('#') ? item.color : Colors.primaryDark,
                                    backgroundColor: item.color.includes('#') ? item.color : Colors.primaryDark
                                }}
                            >
                                <Text numberOfLines={1} style={{
                                    flex: 1,
                                    color: Colors.onPrimary,
                                    fontSize: 16,
                                    paddingLeft: 10,
                                    paddingRight: 10,
                                    alignSelf: 'center',
                                    fontWeight: 'bold'
                                }}>
                                    {item.title}
                                </Text>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    paddingRight: 10
                                }}>
                                    <Text style={{
                                        color: Colors.onPrimary,
                                        fontSize: 16,
                                        paddingRight: 5
                                    }}>
                                        {item.room}
                                    </Text>
                                    <FontAwesome5Icon name={'map-marker-alt'}
                                                      size={16}
                                                      color={Colors.onPrimary}/>
                                    {
                                        item.custom && !_.isNil(removeCustomEntry)
                                            ? <FontAwesome5Icon name={'trash-alt'}
                                                                size={16}
                                                                style={{
                                                                    marginLeft: 15
                                                                }}
                                                                color={Colors.onPrimary}
                                                                onPress={() => {
                                                                    Alert.alert(
                                                                        'Eintrag löschen',
                                                                        'Möchtest du den Eintrag wirklich löschen?',
                                                                        [
                                                                            {
                                                                                text: 'Abbrechen'
                                                                            },
                                                                            {
                                                                                text: 'Löschen',
                                                                                onPress: () => this.props.removeCustomEntry(item)
                                                                            },
                                                                        ],
                                                                    )
                                                                }}
                                            />
                                            : null
                                    }
                                </View>
                            </View>
                            <View style={{
                                backgroundColor: Colors.background,
                                paddingBottom: 3,
                                shadowOpacity: 15,
                                elevation: 15,
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    paddingLeft: 10,
                                    paddingVertical: 6,
                                    justifyContent: 'space-between'
                                }}>
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                    }}>
                                        <FontAwesome5Icon name={'clock'}
                                                          size={14}
                                                          color={Colors.onBackground}/>
                                        <Text numberOfLines={1} style={{
                                            color: Colors.onBackground,
                                            fontSize: 14,
                                            paddingLeft: 10
                                        }}>
                                            {startFormattedTime + ' - ' + endFormattedTime}
                                        </Text>
                                    </View>
                                    {
                                        item.custom && !_.isNil(editCustomEntry)
                                            ? <FontAwesome5Icon name={'edit'}
                                                                size={16}
                                                                style={{
                                                                    marginRight: 7
                                                                }}
                                                                color={Colors.onBackground}
                                                                onPress={() => {
                                                                    this.props.editCustomEntry(item)
                                                                }}
                                            />
                                            : null
                                    }
                                </View>
                                {
                                    item.instructor
                                        ? <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            paddingLeft: 11,
                                            paddingTop: 3,
                                            paddingBottom: 3
                                        }}>
                                            <FontAwesome5Icon name={'user-tie'}
                                                              size={14}
                                                              color={Colors.onBackground}/>
                                            <Text numberOfLines={1} style={{
                                                color: Colors.onBackground,
                                                fontSize: 14,
                                                paddingLeft: 10
                                            }}>
                                                {item.instructor}
                                            </Text>
                                        </View>
                                        : null
                                }
                                {
                                    item.description
                                        ? <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            paddingLeft: 10,
                                            paddingTop: 3,
                                            paddingBottom: 3
                                        }}>
                                            <FontAwesome5Icon name={'info-circle'}
                                                              size={14}
                                                              color={Colors.onBackground}/>
                                            <Text style={{
                                                color: Colors.onBackground,
                                                fontSize: 14,
                                                paddingLeft: 10
                                            }}>
                                                {item.description}
                                            </Text>
                                        </View>
                                        : null
                                }
                                {
                                    item.remarks
                                        ? <Text style={{
                                            color: Colors.onBackground,
                                            fontSize: 14,
                                            paddingLeft: 10,
                                            paddingTop: 3,
                                            paddingBottom: 6
                                        }}>
                                            {item.remarks}
                                        </Text>
                                        : null
                                }
                            </View>
                        </View>
                    })
                }
            </ScrollView>
        </Modal>
    }
}
