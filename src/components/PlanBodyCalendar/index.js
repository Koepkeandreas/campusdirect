import React, {Component} from 'react';
import {View} from 'react-native';
import {Styles} from "../../utils/styles";
import TimeLine from "../TimeLine";
import * as _ from "lodash";
import {timeLine} from "../../utils";
import PlanItem from "../PlanItem";
import PlanDetailModal from "../PlanDetailModal";

type Props = {
    Colors: Object,
    height: number,
    showTimeLine?: boolean,
    showHalfLine?: boolean,
    showItems?: boolean,
    plan?: Array,
    date?: any,
    style?: any
}

type State = {
    modalItems: Array,
    visible: boolean
}

export default class PlanBodyCalendar extends Component<Props, State> {
    state = {
        modalItems: [],
        visible: false
    };

    componentDidMount(): void {

    }

    render() {
        const {Colors, height, showTimeLine, showHalfLine, showItems, plan, date, style} = this.props;
        const {modalItems, visible} = this.state;

        const dayStartTime = Math.round(date.setHours(_.first(timeLine), 0, 0) / 1000);
        const dayEndTime = Math.round(date.setHours(_.last(timeLine) + 1, 0, 0) / 1000);
        const dayTimeRange = dayEndTime - dayStartTime;

        const h = (dayTimeRange / timeLine.length + 1) * height / dayTimeRange;

        return (
            <View style={[Styles.flex, {alignItems: 'center'}, style]}>
                {
                    _.map(timeLine, (el, index) => {

                        return <View key={index} style={{
                            width: '100%',
                            height: h,
                            borderBottomWidth: timeLine.length === index + 1 ? 0 : 1,
                            borderColor: Colors.border,
                            justifyContent: 'center'
                        }}>
                            {
                                showHalfLine
                                    ? <View style={{
                                        width: '100%',
                                        height: 1,
                                        borderBottomWidth: 0.4,
                                        borderColor: Colors.border
                                    }}/>
                                    : null
                            }
                        </View>
                    })
                }
                {
                    showTimeLine
                        ? <TimeLine Colors={Colors} height={height}/>
                        : null
                }
                {
                    showItems && plan && date
                        ? <PlanItem
                            Colors={Colors}
                            plan={plan}
                            height={height}
                            date={date}
                            openPlanDetailModal={(items) => this.setState({
                                modalItems: items,
                                visible: true
                            })}
                        />
                        : null
                }
                <PlanDetailModal Colors={Colors}
                                 visible={visible}
                                 onClose={() => this.setState({visible: false, modalItems: []})}
                                 items={modalItems}
                />
            </View>
        );
    }
}
