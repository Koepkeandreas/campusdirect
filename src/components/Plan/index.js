import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native';
import PlanHeader from "../PlanHeader";
import PlanBody from "../PlanBody";
import AsyncStorage from "@react-native-community/async-storage";
import {fetchPlan} from "../../utils";
import dayjs from 'dayjs';
import _ from 'lodash';
import TipHomeModal from "../TipHomeModal";

type Props = {
    Colors: Object,
    campusIndex: string,
    studyNumber: string,
    campusDualHash: string,
    bautzenHash: string,
    showTips: boolean
}

type State = {
    loading: boolean,
    updating: boolean,
    studyPlan: Array,
    customPlan: Array,
    tipVisible: boolean
}

export default class Plan extends Component<Props, State> {
    state = {
        loading: true,
        updating: true,
        studyPlan: [],
        customPlan: [],
        tipVisible: false
    };

    async componentDidMount(): void {
        const {bautzenHash, campusDualHash, campusIndex, studyNumber, showTips} = this.props;

        if (this.state.loading) {
            let customPlan = await AsyncStorage.getItem('customPlan');
            if (customPlan !== null) {
                try {
                    customPlan = JSON.parse(customPlan);
                } catch (e) {
                    customPlan = [];
                }
            } else customPlan = [];

            let studyPlan = await AsyncStorage.getItem('studyPlan');
            if (studyPlan !== null) {
                try {
                    studyPlan = JSON.parse(studyPlan);
                } catch (e) {
                    studyPlan = [];
                }
            } else studyPlan = [];

            let studyPlanFilter = await AsyncStorage.getItem('studyPlanFilter');
            if (studyPlanFilter !== null) {
                try {
                    studyPlanFilter = JSON.parse(studyPlanFilter);
                } catch (e) {
                    studyPlanFilter = [];
                }
            }

            if (studyPlanFilter && _.isArray(studyPlanFilter) && studyPlanFilter.length > 0) {
                studyPlan = _.filter(studyPlan, (entry: Object) => _.indexOf(studyPlanFilter, entry.title) !== -1)
            }

            this.setState({
                loading: false,
                customPlan,
                studyPlan
            });

            if (this.state.updating) {
                setTimeout(() => {
                    fetchPlan(campusIndex, studyNumber, campusDualHash, bautzenHash, null, async (plan: string) => {
                        try {
                            let studyPlan = JSON.parse(plan);

                            studyPlan = _.map(studyPlan, (el) => {
                                if(typeof el.start === 'string' && el.start.match(/[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}/gm)){
                                    el.start = dayjs(el.start, 'YYYY-MM-DD HH:mm').unix();
                                    el.end = dayjs(el.end, 'YYYY-MM-DD HH:mm').unix();
                                }

                                return el;
                            });

                            await AsyncStorage.setItem('studyPlan', JSON.stringify(studyPlan));
                            await AsyncStorage.setItem('firstStart', 'false');

                            let oldModuleList = await AsyncStorage.getItem('studyPlanModuleList');
                            if (oldModuleList !== null) {
                                try {
                                    oldModuleList = JSON.parse(oldModuleList);
                                } catch (e) {
                                    oldModuleList = [];
                                }
                            }
                            const studyPlanModuleList = _.uniq(_.map(studyPlan, (entry) => entry.title));
                            await AsyncStorage.setItem('studyPlanModuleList', JSON.stringify(studyPlanModuleList));

                            if (studyPlanFilter === null || studyPlanFilter.length === 0) {
                                studyPlanFilter = studyPlanModuleList;
                                await AsyncStorage.setItem('studyPlanFilter', JSON.stringify(studyPlanFilter));
                            } else {
                                const newModule = _.filter(studyPlanModuleList, (el) => _.indexOf(oldModuleList, el) === -1);
                                _.map(newModule, (el) => {
                                    studyPlanFilter.push(el)
                                });
                                await AsyncStorage.setItem('studyPlanFilter', JSON.stringify(studyPlanFilter));
                            }

                            studyPlan = _.filter(studyPlan, (entry: Object) => _.indexOf(studyPlanFilter, entry.title) !== -1);
                            studyPlan = _.sortBy(studyPlan, 'start', 'desc');

                            this.setState({
                                updating: false,
                                studyPlan
                            })
                        } catch (e) {
                            alert('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
                        }
                    }, () => {
                        this.setState({
                            updating: false
                        })
                    })
                }, 100);
            }

            setTimeout(() => this.setState({
                tipVisible: showTips
            }),2000);
        }
    };

    render() {
        const {Colors, showTips} = this.props;
        const {loading, updating, studyPlan, customPlan, tipVisible} = this.state;

        const plan = [
            ...studyPlan,
            ...customPlan
        ];

        return <View style={{
            flex: 1
        }}
        >
            <PlanHeader
                Colors={Colors}
                plan={plan}
            />
            <PlanBody
                Colors={Colors}
                plan={plan}
            />
            {
                updating
                    ? <View style={{
                        position: 'absolute',
                        right: 10,
                        top: 3,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                        <ActivityIndicator size="small" color={Colors.primary}/>
                    </View>
                    : null
            }
            {
                showTips
                    ? <TipHomeModal visible={tipVisible} onClose={() => this.setState({tipVisible: false})}
                                    Colors={Colors}/>
                    : null
            }
        </View>
    }
}
