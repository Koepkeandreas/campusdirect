import React, {Component} from 'react';
import {ScrollView, Text, View} from 'react-native';
import Modal from "react-native-modal";
import _ from "lodash";
import LinearGradient from "react-native-linear-gradient";

type Props = {
    visible: boolean,
    onClose: Function,
    data: Array,
    Colors: Object
}

export default class ReminderLatestModal extends Component<Props> {
    render() {
        const {visible, onClose, data, Colors} = this.props;

        return (
            <Modal
                backdropOpacity={0.3}
                isVisible={visible}
                disableAnimation={true}
                onBackdropPress={() => onClose()}
                onBackButtonPress={() => onClose()}
                style={{alignItems: 'center'}}
            >
                <View style={{
                    width: '90%',
                    backgroundColor: Colors.background,
                    shadowOpacity: 5,
                    shadowRadius: 15,
                    elevation: 15,
                    borderWidth: 1,
                    borderColor: Colors.border
                }}>
                    <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                    style={{
                                        height: 30,
                                        justifyContent: 'center',
                                        backgroundColor: Colors.primary
                                    }}
                    >
                        <Text style={{
                            fontSize: 18,
                            fontWeight: 'bold',
                            color: Colors.onPrimary,
                            paddingLeft: 10
                        }}>
                            {'Ergebnisse'}
                        </Text>
                    </LinearGradient>
                    <View style={{
                        padding: 5,
                        maxHeight: 200
                    }}>
                        <ScrollView>
                            <View style={{
                                width: '100%'
                            }}>
                                {
                                    _.map(data, (el, index) => {
                                        const title = _.get(el, 'AWOBJECT', '');
                                        const date = _.get(el, 'BOOKDATE', '');
                                        const grade = _.get(el, 'GRADESYMBOL', '');

                                        const year = date.substring(0, 4);
                                        const month = date.substring(4, 6);
                                        const day = date.substring(6, 8);

                                        return <View key={index} style={{
                                            padding: 10,
                                            borderBottomWidth: data.length === index + 1 ? 0 : 0.2,
                                        }}>
                                            <Text numberOfLines={1} style={{
                                                fontSize: 16,
                                                fontWeight: 'bold',
                                                color: Colors.onBackground
                                            }}>
                                                {title}
                                            </Text>
                                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <Text numberOfLines={1} style={{
                                                        fontSize: 16,
                                                        color: Colors.onBackground
                                                    }}>
                                                        {'Note'}: {grade}
                                                    </Text>
                                                </View>
                                                <Text numberOfLines={1} style={{
                                                    fontSize: 16,
                                                    color: Colors.onBackground
                                                }}>
                                                    {day}.{month}.{year}
                                                </Text>
                                            </View>
                                        </View>
                                    })
                                }
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </Modal>
        );
    }
}

