import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import {monthNames} from "../../utils";

type Props = {
    Colors: Object,
    date: number,
    left: Function,
    middle: Function,
    right: Function
}

type State = {}

export default class WeekPlanHeader extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, date, right, left, middle} = this.props;
        const {} = this.state;

        const dateNow = new Date(date);
        const firstDay = dateNow.getDate() - dateNow.getDay() + (dateNow.getDay() === 0 ? -6 : 1);
        const firstDate = new Date(dateNow.setDate(firstDay));
        const lastDay = dateNow.getDate() - (dateNow.getDay() - 1) + 6;
        const lastDate = new Date(dateNow.setDate(lastDay));

        const firstMonth = firstDate.getMonth();
        const lastMonth = lastDate.getMonth();

        const title = firstMonth === lastMonth ? `${firstDate.getDate()}.  -  ${lastDate.getDate()}. ${monthNames[lastMonth]}` : `${firstDate.getDate()}. ${monthNames[firstMonth]}   -   ${lastDate.getDate()}. ${monthNames[lastMonth]}`;

        return <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                               style={{
                                   height: 40,
                                   flexDirection: 'row',
                                   justifyContent: 'space-between',
                                   alignItems: 'center'
                               }}
        >
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 50,
                width: '17%',
                paddingLeft: 10
            }}
                              onPress={() => left()}
            >
                <FontAwesome5 name={'chevron-left'} size={17} color={Colors.onPrimary}/>
            </TouchableOpacity>
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 200,
                alignItems: 'center',
                width: '40%'
            }}
                              onPress={() => middle()}
            >
                <Text numberOfLines={1} style={{
                    color: Colors.onPrimary,
                    fontSize: 16
                }}>
                    {
                        title
                    }
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 50,
                alignItems: 'flex-end',
                width: '17%',
                paddingRight: 10
            }}
                              onPress={() => right()}
            >
                <FontAwesome5 name={'chevron-right'} size={17} color={Colors.onPrimary}/>
            </TouchableOpacity>
        </LinearGradient>
    }
}
