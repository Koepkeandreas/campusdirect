import React, {Component} from 'react';
import {Dimensions, Image, Text, View} from 'react-native';
import {Styles} from "../../utils/styles";
import * as _ from "lodash";
import { Tooltip } from 'react-native-elements';
import Lightbox from 'react-native-lightbox';

let {width, height} = Dimensions.get('window');

type Props = {
    Colors: Object,
    meals: Array,
    loading: boolean
}

type State = {}

export default class MensaBody extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, meals, loading} = this.props;
        const {} = this.state;

        return loading
            ? <View style={{flex: 1, backgroundColor: Colors.background}}/>
            : <View style={[Styles.flex, Styles.center]}>
                {
                    _.isEmpty(meals)
                        ? <Text style={{color: Colors.onBackground, fontSize: 14}}>
                            {'Heute gibt es kein Essen'}
                        </Text>
                        : _.map(meals, (meal, index) => {

                            const notes = _.map(_.get(meal, 'notes', []), (note, index) => {
                                let parsedNote = '';

                                if (note.includes('Laktose')) parsedNote = '  Laktose';
                                else if (note.includes('Schwein')) parsedNote = '  Schwein';
                                else if (note.includes('Rind')) parsedNote = '  Rind';
                                else if (note.includes('vegan')) parsedNote = '  Vegan';
                                else if (note.includes('vegetarisch')) parsedNote = '  Vegetarisch';

                                return <Text key={index}
                                             style={{color: Colors.onBackground, fontSize: 11}}>{parsedNote}</Text>
                            });

                            const name = _.get(meal, 'name', '');
                            let price = _.get(meal, 'prices.Studierende:', 0);
                            if(price === 0) price = _.get(meal, 'prices.Studierende', 0);
                            if (String(price).length <= 4) price = price.toFixed(2) + ' €';
                            let img = _.get(meal, 'image', null);
                            if (img === "https://static.studentenwerk-dresden.de/bilder/mensen/studentenwerk-dresden-lieber-mensen-gehen.jpg") img = null;

                            const fl = ((height-350)/meals.length) * (width*0.5);
                            const textFl = 15*13*name.length;

                            let numberOfLines = 4;
                            if(fl/textFl < 1.1) numberOfLines = 3;
                            if(fl/textFl < 0.7) numberOfLines = 2;
                            if(fl/textFl < 0.6) numberOfLines = 1;

                            return <View key={index} style={{
                                flex: 1,
                                borderBottomWidth: meals.length === index + 1 ? 0 : 1,
                                borderColor: Colors.border
                            }}>
                                <View style={{
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flexDirection: 'row'
                                }}>
                                    {
                                        !_.isNil(img)
                                            ?  <Lightbox activeProps={{
                                                width: '100%',
                                                height: '50%'
                                            }}>
                                                <Image source={{uri: img}}
                                                     resizeMode={'cover'}
                                                     style={{width: width*0.4, height: (height-400)/meals.length, marginLeft: 10}}/>
                                            </Lightbox>
                                            : null
                                    }
                                    <View style={{
                                        flex: 1,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginHorizontal: 10
                                    }}>
                                        <Tooltip
                                            backgroundColor={Colors.background}
                                            width={width*0.8}
                                            height={height*0.3}
                                            popover={<Text style={{
                                                fontSize: 15,
                                                color: Colors.onBackground,
                                                textAlign: 'center'
                                            }}>{name}</Text>}>
                                            <Text numberOfLines={numberOfLines} style={{
                                                fontSize: 15,
                                                color: Colors.onBackground,
                                                textAlign: 'center'
                                            }}>
                                                {name}
                                            </Text>
                                        </Tooltip>
                                    </View>
                                </View>
                                <View style={{
                                    width: '100%',
                                    height: 20,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    paddingHorizontal: 5
                                }}>
                                    <View style={{
                                        flexDirection: 'row',
                                        flex: 1,
                                        flexWrap: 'wrap'
                                    }}>
                                        <Text numberOfLines={2}>
                                            {
                                                notes
                                            }
                                        </Text>
                                    </View>
                                    <Text style={{
                                        fontSize: 12,
                                        color: Colors.onBackground,
                                        marginRight: 5
                                    }}>
                                        {
                                            price === 0
                                                ? 'Ausverkauft'
                                                : price
                                        }
                                    </Text>
                                </View>
                            </View>
                        })
                }
            </View>
    }
}
