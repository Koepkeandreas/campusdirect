import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {CheckBox} from 'react-native-elements';
import _ from 'lodash';
import AsyncStorage from "@react-native-community/async-storage";

import {_goToURL, CampusList} from '../../utils';
import {Styles} from "../../utils/styles";
import Form from './../Form';
import LinearGradient from "react-native-linear-gradient";

type Props = {
    navigation: any,
    screenProps: {
        Colors: Object,
        campusIndex: String,
        privacyAccept: Boolean,
        reload: Function
    }
}

type State = {
    campusIndex: String,
    privacyAccept: Boolean
}

export default class CampusPicker extends Component<Props, State> {
    state = {
        campusIndex: this.props.screenProps.campusIndex || 'ba_dd',
        privacyAccept: this.props.screenProps.privacyAccept
    };

    onSubmit = async () => {
        const {privacyAccept, campusIndex} = this.state;

        if (privacyAccept) {
            let mensa = _.find(CampusList, {value: campusIndex}).mensa;
            if (!mensa) mensa = 32;

            await AsyncStorage.setItem('campusIndex', campusIndex);
            await AsyncStorage.setItem('mensa', String(mensa));
            await AsyncStorage.setItem('privacyAccept', privacyAccept.toString());

            this.props.screenProps.reload();
        } else {
            alert('Bitte akzeptiere die Datenschutzbedingungen!')
        }
    };

    render() {
        const {campusIndex, privacyAccept} = this.state;
        const {Colors} = this.props.screenProps;

        const Attributes = [
            {
                type: 'dropdown',
                placeholder: 'Uni',
                data: CampusList,
                defaultValue: campusIndex,
                containerStyle: {
                    width: '80%',
                    minWidth: 250
                },
                itemColor: Colors.onBackground,
                selectedItemColor: Colors.primary,
                onChange: (value) => this.setState({campusIndex: value}),
                textColor: Colors.onBackground,
                baseColor: Colors.onBackground,
                pickerStyle: {backgroundColor: Colors.background}
            },
            {
                type: 'view',
                style: {
                    width: '80%',
                    minWidth: 250
                },
                child: <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <CheckBox
                        checked={privacyAccept}
                        onPress={() => this.setState({privacyAccept: !privacyAccept})}
                        containerStyle={{width: '20%'}}
                        checkedColor={Colors.primary}
                    />
                    <Text style={{width: '80%', color: Colors.onBackground}}>
                        {'Indem du auf WEITER klickst, akzeptierst du unsere '}
                        <Text style={{color: Colors.primary}}
                              onPress={() => _goToURL('http://campusdirectmobile.de/#/datenschutz')}>
                            {'Datenschutzbedingungen'}
                        </Text>
                    </Text>
                </View>
            },
            {
                type: 'submit',
                placeholder: 'WEITER',
                raised: true,
                onSubmit: () => this.onSubmit(),
                containerStyle: {
                    width: '80%',
                    marginTop: 10,
                    minWidth: 250
                },
                buttonStyle: {backgroundColor: Colors.primary},
                titleStyle: {color: Colors.onPrimary},
                ViewComponent: LinearGradient,
                linearGradientProps: {
                    colors: [Colors.primaryDark, Colors.primary],
                    start: {x: 1, y: 1},
                    end: {x: 0, y: 0},
                }
            }
        ];

        return (
            <View style={[Styles.flex, {backgroundColor: Colors.background}]}>
                <Form form={Attributes}
                      formStyle={[Styles.flex, Styles.center]}
                />
            </View>
        );
    }
}
