import React, {Component} from 'react';
import {Dimensions, TouchableOpacity} from 'react-native';
import {Dropdown} from "react-native-material-dropdown";
import LinearGradient from "react-native-linear-gradient";
import {Styles} from "../../utils/styles";
import _ from "lodash";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

let {height} = Dimensions.get('window');

type Props = {
    mensa: number,
    onChange: Function,
    Colors: Object,
    mensaList: Array,
    onListUpdate: Function
}

type State = {}

export default class MensaPicker extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {mensa, onChange, Colors, mensaList, onListUpdate} = this.props;
        const {} = this.state;

        let List = [...mensaList];
        const entry = _.find(List, {id: mensa});
        List.splice(_.findIndex(List, {id: mensa}), 1);
        List = _.sortBy(List, ['city', 'name'], 'desc');
        List.unshift(entry);

        return <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                               style={[Styles.center, {
                                   height: 60,
                                   width: '100%',
                                   flexDirection: 'row',
                                   paddingHorizontal: 10
                               }]}
        >
            <Dropdown data={List}
                      value={mensa}
                      label={'Mensa'}
                      containerStyle={{
                          flex: 1
                      }}
                      itemColor={Colors.onBackground}
                      selectedItemColor={Colors.primary}
                      onChangeText={(value) => onChange(value)}
                      textColor={Colors.onPrimary}
                      baseColor={Colors.onPrimary}
                      valueExtractor={({id}) => id}
                      labelExtractor={({name, city}) => {
                          if (!_.isNil(city)) {
                              return city + ', ' + name
                          }
                          return name;
                      }}
                      pickerStyle={{
                          shadowOpacity: 10,
                          elevation: 10,
                          backgroundColor: Colors.background
                      }}
                      itemCount={8}
            />
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                width: 50,
                paddingLeft: 10
            }}
                              onPress={() => onListUpdate()}
            >
                <FontAwesome5 name={'sync'}
                              size={17}
                              color={Colors.onPrimary}
                />
            </TouchableOpacity>
        </LinearGradient>
    }
}
