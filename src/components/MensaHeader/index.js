import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

type Props = {
    Colors: Object,
    date: any,
    left: Function,
    right: Function,
    middle: Function
}

export default class MensaHeader extends Component<Props, State> {

    componentDidMount(): void {

    }

    render() {
        const {Colors, date, right, left, middle} = this.props;

        const dateText = date.format('dddd, DD. MMMM');

        return <LinearGradient colors={[Colors.primary, Colors.primaryDark]}
                               style={{
                                   width: '100%',
                                   height: 40,
                                   flexDirection: 'row',
                                   justifyContent: 'space-between',
                                   alignItems: 'center'
                               }}
        >
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 50,
                width: '17%',
                paddingLeft: 10
            }}
                              onPress={left}
            >
                <FontAwesome5 name={'chevron-left'}
                              size={17}
                              color={Colors.onPrimary}
                />
            </TouchableOpacity>
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 200,
                alignItems: 'center',
                width: '40%'
            }}
                              onPress={middle}
            >
                <Text style={{
                    color: Colors.onPrimary,
                    fontSize: 16
                }}>
                    {
                        dateText
                    }
                </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{
                height: '100%',
                justifyContent: 'center',
                minWidth: 50,
                alignItems: 'flex-end',
                width: '17%',
                paddingRight: 10
            }}
                              onPress={right}
            >
                <FontAwesome5 name={'chevron-right'}
                              size={17}
                              color={Colors.onPrimary}
                />
            </TouchableOpacity>
        </LinearGradient>
    }
}
