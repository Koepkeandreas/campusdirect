import React, {Component} from 'react';
import {Dimensions, Text, View} from 'react-native';
import Picture from "../Picture";
import {getNearestDate, weekdayNames} from "../../utils";

let WIDTH = Dimensions.get('window').width;

type Props = {
    Colors: Object,
    plan: Array
}

type State = {}

export default class PlanHeader extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    };

    render() {
        const {Colors, plan} = this.props;
        const {} = this.state;

        const todayDate = `${new Date().getDate()}.${new Date().getMonth() + 1}.${new Date().getFullYear()}`;
        const nextDateStamp = getNearestDate(plan);
        let nextDate = null,
            nextDateName = null;
        if (nextDateStamp) {
            nextDate = `${nextDateStamp.getDate()}.${nextDateStamp.getMonth() + 1}.${nextDateStamp.getFullYear()}`;
            nextDateName = weekdayNames[nextDateStamp.getDay()];
        }

        const styles = {
            trapezoid: {
                position: 'absolute',
                left: (WIDTH / 2) - (WIDTH / 2.5) / 2,
                width: WIDTH / 2.5,
                height: 0,
                borderTopWidth: 39,
                borderLeftWidth: 50,
                borderLeftColor: 'transparent',
                borderRightWidth: 50,
                borderRightColor: 'transparent',
                borderStyle: 'solid'
            },
            dayName: {
                fontSize: 12,
                color: Colors.onBackground
            },
            date: {
                fontSize: 16,
                color: Colors.onBackground
            }
        };

        return <View>
            <View style={{
                marginTop: 25,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginHorizontal: 10,
                paddingHorizontal: 10,
                borderTopWidth: 1,
                borderRightWidth: 1,
                borderLeftWidth: 1,
                borderTopEndRadius: 5,
                borderTopRightRadius: 5,
                borderTopStartRadius: 5,
                borderTopLeftRadius: 5,
                borderColor: Colors.border,
                backgroundColor: Colors.PlanBackgroundTrans
            }}>
                <View style={{width: '30%'}}>
                    <Text style={styles.dayName}>
                        {'Heute'}
                    </Text>
                    <Text style={styles.date}>
                        {todayDate}
                    </Text>
                </View>
                {
                    !nextDateStamp
                        ? <View style={{width: '30%', justifyContent: 'center'}}>
                            <Text style={[styles.dayName, {textAlign: 'right'}]}>
                                {'Keine Daten in den nächten Tagen'}
                            </Text>
                        </View>
                        : <View style={{width: '30%'}}>
                            <Text style={[styles.dayName, {textAlign: 'right'}]}>
                                {nextDateName}
                            </Text>
                            <Text style={[styles.date, {textAlign: 'right'}]}>
                                {nextDate}
                            </Text>
                        </View>
                }
            </View>
            <View style={[styles.trapezoid, {top: 25, borderTopColor: Colors.border}]}/>
            <View style={[styles.trapezoid, {top: 24, borderTopColor: Colors.background}]}/>
            <Picture type={'logo_foreground_blue'}
                     imgWidth={80}
                     style={{position: 'absolute', left: WIDTH / 2 - 40, top: -10}}
                     Colors={Colors}
            />
        </View>
    }
}
