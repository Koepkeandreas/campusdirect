import React, {Component} from 'react';
import {View} from 'react-native';
import ProfileBodyDataView from "../ProfileBodyDataView";
import ProfileBodyChartView from "../ProfileBodyChartView";

type Props = {
    Colors: Object,
    profile: Object
}

type State = {}

export default class ProfileBody extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, profile} = this.props;
        const {semester, totalExams, successExams, failureExams, totalModules, successModules, credits} = profile;

        const progressExams = successExams > 0 ? (successExams - failureExams) / successExams : 0;
        const progressModules = totalModules > 0 ? successModules / totalModules : 0;

        return <View style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: Colors.background,
            paddingBottom: 10
        }}>
            <View style={{
                width: '55%',
                alignItems: 'center',
                paddingTop: 5,
                paddingLeft: 10,
                paddingRight: 5,
                paddingBottom: 5
            }}>
                <ProfileBodyDataView label={'Semester'}
                                     data={semester}
                                     Colors={Colors}
                />
                <ProfileBodyChartView label={'Prüfungen'}
                                      data={totalExams}
                                      text={`${successExams} erfolgreich` + '\n' + `${failureExams} durchgefallen`}
                                      progress={progressExams}
                                      Colors={Colors}
                />
            </View>
            <View style={{
                width: '45%',
                alignItems: 'center',
                paddingTop: 5,
                paddingLeft: 5,
                paddingRight: 10,
                paddingBottom: 5
            }}>
                <ProfileBodyChartView label={'Module'}
                                      data={totalModules}
                                      text={`${successModules} abgeschlossen`}
                                      progress={progressModules}
                                      Colors={Colors}
                />
                <ProfileBodyDataView label={'Credits'}
                                     data={credits}
                                     Colors={Colors}
                />
            </View>
        </View>
    }
}
