import React, {Component} from 'react';
import SelectMultiple from "react-native-select-multiple";
import Modal from "react-native-modal";
import {Text, View} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {Button} from "react-native-elements";

type Props = {
    calendarFilterList: Array,
    selectedItems: Array,
    visible: boolean,
    onClose: Function,
    onFilterChange: Function,
    Colors: Object
}

type State = {
    selectedItems: Array
}

export default class FilterModal extends Component<Props, State> {
    state = {
        selectedItems: this.props.selectedItems
    };

    componentDidMount(): void {

    }

    onClose = () => {
        this.setState({
            selectedItems: this.props.selectedItems
        });
        this.props.onClose();
    };

    render() {
        const {calendarFilterList, visible, onClose, Colors, onFilterChange} = this.props;
        const {selectedItems} = this.state;

        return <Modal
            backdropOpacity={0.3}
            isVisible={visible}
            disableAnimation={true}
            onBackdropPress={() => this.onClose()}
            onBackButtonPress={() => this.onClose()}
            style={{alignItems: 'center'}}
        >
            <View style={{
                width: '90%',
                backgroundColor: Colors.background,
                borderRadius: 3,
                shadowOpacity: 15,
                elevation: 15,
                marginBottom: 5,
                marginTop: 5,
                borderWidth: 1,
                borderColor: Colors.border
            }}>
                <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                style={{
                                    height: 25,
                                    justifyContent: 'space-between',
                                    backgroundColor: Colors.primary,
                                    flexDirection: 'row',
                                    borderWidth: 1,
                                    borderColor: Colors.primary
                                }}
                >
                    <Text style={{
                        color: Colors.onPrimary,
                        fontWeight: 'bold',
                        fontSize: 16,
                        paddingLeft: 10
                    }}>
                        Filter
                    </Text>
                </LinearGradient>
                <SelectMultiple
                    items={calendarFilterList}
                    selectedItems={selectedItems}
                    style={{
                        maxHeight: 350
                    }}
                    rowStyle={{
                        height: 40,
                        backgroundColor: Colors.FilterBackground
                    }}
                    onSelectionsChange={(selectedItems) => this.setState({selectedItems})}
                    labelStyle={{
                        color: Colors.onFilterBackground
                    }}
                />
                <View style={{
                    backgroundColor: Colors.background,
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'center'
                }}>
                    <Button raised={true}
                            title={'Abbrechen'}
                            onPress={() => {
                                this.onClose();
                            }}
                            titleStyle={{
                                color: Colors.onPrimary
                            }}
                            containerStyle={{
                                width: '50%'
                            }}
                            buttonStyle={{
                                borderRadius: 0,
                                height: 35
                            }}
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: [Colors.primaryDark, Colors.primary],
                                start: {x: 0, y: 1},
                                end: {x: 0, y: 0},
                            }}
                    />
                    <Button raised={true}
                            title={'Übernehmen'}
                            onPress={() => {
                                onFilterChange(selectedItems)
                            }}
                            titleStyle={{
                                color: Colors.onPrimary
                            }}
                            containerStyle={{
                                width: '50%'
                            }}
                            buttonStyle={{
                                borderRadius: 0,
                                height: 35
                            }}
                            ViewComponent={LinearGradient}
                            linearGradientProps={{
                                colors: [Colors.primaryDark, Colors.primary],
                                start: {x: 0, y: 1},
                                end: {x: 0, y: 0},
                            }}
                    />
                </View>
            </View>
        </Modal>;
    }
}
