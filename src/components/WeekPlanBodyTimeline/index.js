import React, {Component} from 'react';
import {Text, View} from 'react-native';
import * as _ from "lodash";

type Props = {
    Colors: Object,
    dateList: Array
}

type State = {}

export default class WeekPlanBodyTimeline extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    }

    render() {
        const {Colors, dateList} = this.props;
        const {} = this.state;

        return <View style={{
            height: 20,
            flexDirection: 'row',
            borderBottomWidth: 1,
            borderColor: Colors.border,
            alignItems: 'center',
            backgroundColor: Colors.BackgroundTrans
        }}>
            <View style={{
                width: 25
            }}/>
            <View style={{
                flex: 1,
                flexDirection: 'row',
                color: Colors.onBackground
            }}>
                {
                    _.map(dateList, (el, index) => {
                        return <View key={index}
                                     style={{
                                         alignItems: 'flex-start',
                                         width: index !== 6 ? '15%' : '10%',
                                         minWidth: index !== 6 ? 40 : 35
                                     }}
                        >
                            <Text style={{
                                fontSize: 12,
                                fontWeight: el.today ? 'bold' : null,
                                color: el.today ? Colors.primary : Colors.onBackground
                            }}>
                                {
                                    `${el.title}. ${el.day}`
                                }
                            </Text>
                        </View>
                    })
                }
            </View>
        </View>
    }
}
