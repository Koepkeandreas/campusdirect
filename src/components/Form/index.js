import React, {Component} from 'react';
import {View} from 'react-native';
import {Button, Input} from 'react-native-elements';
import _ from "lodash";
import {Dropdown} from "react-native-material-dropdown";

type Props = {
    formStyle?: Object,
    form: {
        type: String,
        defaultValue?: any,
        placeholder?: String,
        keyboardType?: String,
        onChange?: Function,
        onSubmit?: Function,
        inputContainerStyle?: Object,
        style?: Object,
        inputStyle?: Object,
        label?: String,
        labelStyle?: Object,
        placeholderTextColor?: String,

        raised?: boolean,
        containerStyle?: Object,
        buttonStyle?: Object,
        titleStyle?: Object,
        ViewComponent: any,
        linearGradientProps: Object,

        data?: Array,
        itemColor?: String,
        selectedItemColor?: String,
        textColor?: String,
        baseColor?: String,
        pickerStyle?: String,
        rightIcon?: any,
        rightIconStyle?: Object,
        leftIcon?: any,
        leftIconStyle?: Object,
        onPress?: Function
    }[]
}

export default class Form extends Component<Props> {

    getComponent = (form, key) => {
        switch (form.type) {
            case 'textInput': {
                return <Input
                    placeholder={form.placeholder}
                    placeholderTextColor={form.placeholderTextColor}
                    label={form.label}
                    labelStyle={form.labelStyle}
                    keyboardType={form.keyboardType}
                    onChangeText={(value) => form.onChange(value)}
                    value={form.defaultValue}
                    key={key}
                    inputContainerStyle={form.inputContainerStyle}
                    inputStyle={form.inputStyle}
                    style={form.style}
                    containerStyle={form.containerStyle}
                    leftIcon={form.leftIcon}
                    leftIconStyle={form.leftIconStyle}
                    rightIcon={form.rightIcon}
                    rightIconStyle={form.rightIconStyle}
                />
            }
            case 'submit': {
                return <Button title={form.placeholder}
                               key={key}
                               raised={form.raised}
                               onPress={() => form.onSubmit()}
                               containerStyle={form.containerStyle}
                               buttonStyle={form.buttonStyle}
                               titleStyle={form.titleStyle}
                               ViewComponent={form.ViewComponent}
                               linearGradientProps={form.linearGradientProps}
                />
            }
            case 'button': {
                return <Button title={form.placeholder}
                               key={key}
                               raised={form.raised}
                               onPress={() => form.onPress()}
                               containerStyle={form.containerStyle}
                               buttonStyle={form.buttonStyle}
                               titleStyle={form.titleStyle}
                               ViewComponent={form.ViewComponent}
                               linearGradientProps={form.linearGradientProps}
                />
            }
            case 'dropdown': {
                return <Dropdown label={form.placeholder}
                                 key={key}
                                 data={form.data}
                                 value={form.defaultValue}
                                 containerStyle={form.containerStyle}
                                 itemColor={form.itemColor ? form.itemColor : '#000'}
                                 selectedItemColor={form.selectedItemColor ? form.selectedItemColor : '#000'}
                                 onChangeText={(value) => form.onChange(value)}
                                 textColor={form.textColor ? form.textColor : '#000'}
                                 baseColor={form.baseColor ? form.baseColor : '#000'}
                                 pickerStyle={form.pickerStyle}
                />
            }
            default: {
                return <View style={form.style} key={key}>
                    {form.child}
                </View>
            }
        }
    };

    render() {
        const {form, formStyle} = this.props;

        return (
            <View style={formStyle ? formStyle : {}}>
                {
                    _.map(form, (el, index) => {
                        return this.getComponent(el, index)
                    })
                }
            </View>
        );
    }
}
