import React, {Component} from 'react';
import {Text, View} from 'react-native';
import * as Progress from "react-native-progress";

type Props = {
    label: String,
    data: number,
    text: String,
    progress: number,
    Colors: Object
}

type State = {
    height: number,
    width: number
}

export default class ProfileBodyChartView extends Component<Props, State> {
    state = {
        height: 1,
        width: 1
    };

    componentDidMount(): void {

    }

    render() {
        const {label, data, text, progress, Colors} = this.props;
        const {height, width} = this.state;

        const styles = {
            container: {
                width: '100%',
                height: '70%',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: Colors.border,
                backgroundColor: Colors.PlanBackground,
                marginVertical: 5
            },
            label: {
                height: 25,
                marginLeft: 5,
                width: '100%',
                justifyContent: 'center'
            },
            labelText: {
                fontSize: 14,
                color: Colors.onBackground
            },
            data: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            },
            dataText: {
                fontSize: 20,
                fontWeight: 'bold',
                color: Colors.onBackground
            },
            text: {
                width: '100%',
                height: 20,
                marginLeft: 10,
                justifyContent: 'center'
            },
            textText: {
                fontSize: 12,
                color: Colors.onBackground
            }
        };

        let size = height / 2 - 50;
        if (height / width > 1.6) size = size - 15;

        return <View style={styles.container}
                                 onLayout={(event) => {
                                     let {x, y, width, height} = event.nativeEvent.layout;
                                     this.setState({height, width})
                                 }}
        >
            <View style={styles.label}>
                <Text style={styles.labelText}>
                    {label}
                </Text>
            </View>
            <View style={styles.data}>
                <Text style={styles.dataText}>
                    {data}
                </Text>
            </View>
            <View style={styles.text}>
                <Text style={styles.textText}>
                    {text}
                </Text>
            </View>
            <View style={{
                paddingVertical: height / 12.5,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                {
                    height > 1
                        ? <Progress.Circle progress={progress}
                                           color={Colors.primary}
                                           formatText={() => Math.round(progress * 100) + '%'}
                                           borderWidth={0}
                                           showsText={true}
                                           textStyle={{fontSize: 12}}
                                           size={size}
                                           thickness={height / 37.5}
                                           animated={false}
                        />
                        : null
                }
            </View>
        </View>
    }
}
