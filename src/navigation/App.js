import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import React, {Component} from "react";
import {AppState, StatusBar, View} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import * as _ from "lodash";
import LinearGradient from "react-native-linear-gradient";
import RNRestart from "react-native-restart";

import AuthLoading from "../components/AuthLoading";
import Login from "../components/Login";
import Router from "./Router";
import CampusPicker from "../components/CampusPicker";
import {Styles, useTheme} from "../utils/styles";
import Picture from "../components/Picture";
import {
    CampusList,
    decryptBautzenHash,
    decryptCampusDualHash,
    decryptStudyNumber,
    DefaultMensaList,
    encryptBautzenHash,
    encryptCampusDualHash,
    encryptStudyNumber,
} from "../utils";
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import 'dayjs/locale/de'
import weekday from 'dayjs/plugin/weekday';
import packageJson from '../../package.json';

dayjs.locale('de');
dayjs.extend(weekday);
dayjs.extend(customParseFormat);

const newVersion = packageJson.versionCode;

type State = {
    loading: Boolean,
    firstStart: boolean,
    currentVersion: number,
    useDarkTheme: boolean,
    privacyAccept: boolean,
    showTips: boolean,
    campusIndex: string,
    ts: string,
    studyNumber: string,
    campusDualHash: string,
    bautzenHash: string,

    studyPlan: Array,
    reminder: Object,

    profile: Object,

    mensa: number,
    mensaList: Array,

    studyPlanFilter: Array,
    studyPlanModuleList: Array
}

export default class App extends Component<State> {
    state = {
        loading: true,
        appState: AppState.currentState
    };

    componentDidMount(): void {
        this.loadStorage().done();
    }

    componentDidUpdate(): void {
        this.loadStorage().done();
    }

    loadStorage = async () => {
        //AsyncStorage.clear();

        if (this.state.loading) {
            const keys = ['firstStart', 'currentVersion', 'useDarkTheme', 'privacyAccept', 'campusIndex', 'ts', 'studyNumber', 'encryptedStudyNumber', 'campusDualHash', 'encryptedCampusDualHash', 'bautzenHash', 'encryptedBautzenHash', 'mensa', 'showTips', 'mensaList'];
            const storageValues = await AsyncStorage.multiGet(keys);

            let firstStart = _.nth(storageValues, 0)[1];
            if (firstStart !== null) firstStart = firstStart === 'true';
            else {
                firstStart = true;
                await AsyncStorage.setItem('firstStart', String(firstStart));
            }

            let currentVersion = _.nth(storageValues, 1)[1];
            if (currentVersion !== null) currentVersion = parseInt(currentVersion);
            else {
                currentVersion = newVersion;
                await AsyncStorage.setItem('currentVersion', String(currentVersion));
            }

            let useDarkTheme = _.nth(storageValues, 2)[1];
            if (useDarkTheme !== null) useDarkTheme = useDarkTheme === 'true';
            else {
                useDarkTheme = await AsyncStorage.getItem('darkTheme');

                if (useDarkTheme !== null) useDarkTheme = useDarkTheme === 'true';
                else useDarkTheme = false;

                await AsyncStorage.setItem('useDarkTheme', String(useDarkTheme));
            }

            let privacyAccept = _.nth(storageValues, 3)[1];
            if (privacyAccept !== null) privacyAccept = privacyAccept === 'true';
            else {
                privacyAccept = await AsyncStorage.getItem('privacy');

                if (privacyAccept !== null) privacyAccept = privacyAccept === 'true';
                else privacyAccept = false;

                await AsyncStorage.setItem('privacyAccept', String(privacyAccept));
            }

            let campusIndex = _.nth(storageValues, 4)[1];
            if (campusIndex === null) {
                campusIndex = await AsyncStorage.getItem('campus');
                if (campusIndex !== null) {
                    await AsyncStorage.setItem('campusIndex', campusIndex);
                }
            }

            let ts = _.nth(storageValues, 5)[1];
            if (ts === null) {
                ts = new Date().getTime();
                await AsyncStorage.setItem('ts', String(ts));
            } else {
                ts = parseInt(ts);
            }

            let studyNumber = _.nth(storageValues, 6)[1];
            if (studyNumber === null) {
                studyNumber = await AsyncStorage.getItem('matrNr');
                if (studyNumber !== null) {
                    await AsyncStorage.setItem('studyNumber', studyNumber);
                }
            }

            let encryptedStudyNumber = _.nth(storageValues, 7)[1];
            if (encryptedStudyNumber === null && studyNumber !== null) {
                encryptedStudyNumber = await encryptStudyNumber(studyNumber, ts);
                await AsyncStorage.setItem('encryptedStudyNumber', encryptedStudyNumber);
                await AsyncStorage.removeItem('matrNr');
                await AsyncStorage.removeItem('studyNumber');
            }
            if (encryptedStudyNumber !== null) {
                studyNumber = await decryptStudyNumber(encryptedStudyNumber, ts)
            }

            let campusDualHash = _.nth(storageValues, 8)[1];

            let encryptedCampusDualHash = _.nth(storageValues, 9)[1];
            if (encryptedCampusDualHash === null && campusDualHash !== null) {
                encryptedCampusDualHash = await encryptCampusDualHash(campusDualHash, studyNumber, ts);
                await AsyncStorage.setItem('encryptedCampusDualHash', encryptedCampusDualHash);
                await AsyncStorage.removeItem('campusDualHash');
            }
            if (encryptedCampusDualHash !== null) {
                campusDualHash = await decryptCampusDualHash(encryptedCampusDualHash, studyNumber, ts);
            }

            let bautzenHash = _.nth(storageValues, 10)[1];

            let encryptedBautzenHash = _.nth(storageValues, 11)[1];
            if (encryptedBautzenHash === null && bautzenHash !== null) {
                encryptedBautzenHash = await encryptBautzenHash(bautzenHash, ts);
                await AsyncStorage.setItem('encryptedBautzenHash', encryptedBautzenHash);
                await AsyncStorage.removeItem('bautzenHash');
            }
            if (encryptedBautzenHash !== null) {
                bautzenHash = await decryptBautzenHash(encryptedBautzenHash, ts);
            }

            let mensa = _.nth(storageValues, 12)[1];
            if (mensa !== null) {
                try {
                    mensa = parseInt(mensa);
                } catch (e) {
                    if (!_.isNil(campusIndex)) {
                        let mensa = _.find(CampusList, {value: campusIndex}).mensa;
                        await AsyncStorage.setItem('mensa', String(mensa)).done();
                    }
                }
            }
            if ((_.isNil(mensa) || _.isNaN(mensa)) && !_.isNil(campusIndex)) {
                let mensa = _.find(CampusList, {value: campusIndex}).mensa;
                await AsyncStorage.setItem('mensa', String(mensa)).done();
            }

            let showTips = _.nth(storageValues, 13)[1];
            if (showTips !== null) showTips = showTips === 'true';
            else {
                showTips = true;
                await AsyncStorage.setItem('showTips', String(showTips));
            }

            let mensaList = _.nth(storageValues, 14)[1];
            if (mensaList !== null) {
                try {
                    mensaList = JSON.parse(mensaList);
                } catch (e) {
                    mensaList = [];
                }
            } else {
                mensaList = DefaultMensaList;
                await AsyncStorage.setItem('mensaList', JSON.stringify(mensaList));
            }

            this.setState({
                loading: false,
                firstStart,
                currentVersion,
                useDarkTheme,
                privacyAccept,
                campusIndex,
                ts,
                studyNumber,
                campusDualHash,
                bautzenHash,
                mensa,
                showTips,
                mensaList
            })
        }
    };

    render() {
        const {loading, firstStart, currentVersion, useDarkTheme, privacyAccept, campusIndex, studyNumber, campusDualHash, bautzenHash, mensa, showTips, ts, mensaList} = this.state;
        const Colors = useTheme(useDarkTheme);

        const AppContainer = createAppContainer(createSwitchNavigator(
            {
                AuthLoading: AuthLoading,
                CampusPicker: CampusPicker,
                Login: Login,
                App: createStackNavigator({Home: {screen: Router, navigationOptions: {headerShown: false}}}),
            },
            {
                initialRouteName: 'AuthLoading',
            }
        ));

        return <View style={Styles.flex}>
            <StatusBar
                backgroundColor={'#002071'}
                barStyle='light-content'
            />
            {
                loading
                    ? <LinearGradient colors={[Colors.primaryDark, Colors.primary]}
                                      style={[Styles.flex, Styles.center]}>
                        <Picture type={'logo_foreground'}
                                 imgWidth={150}
                                 Colors={Colors}
                        />
                    </LinearGradient>
                    : <AppContainer screenProps={{
                        Colors,
                        firstStart,
                        currentVersion,
                        newVersion,
                        useDarkTheme,
                        privacyAccept,
                        campusIndex,
                        studyNumber,
                        campusDualHash,
                        bautzenHash,
                        mensa,
                        showTips,
                        mensaList,
                        ts,
                        restart: () => {
                            RNRestart.Restart();
                        },
                        reload: () => {
                            this.setState({
                                loading: true
                            })
                        },
                        changeTheme: () => {
                            this.setState({
                                useDarkTheme: !useDarkTheme
                            })
                        }
                    }}/>
            }
        </View>;
    }
}
