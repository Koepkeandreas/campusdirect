import React, {Component} from 'react';
import {View} from 'react-native';
import {createAppContainer} from "react-navigation";
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import Home from '../components/Home';
import WeekPlan from '../components/WeekPlan';
import Profile from '../components/Profile';
import Mensa from '../components/Mensa';
import Settings from '../components/Settings';

type Props = {
    screenProps: {
        Colors: Object,
        firstStart: boolean,
        currentVersion: number,
        newVersion: number,
        useDarkTheme: boolean,
        privacyAccept: boolean,
        campusIndex: string,
        studyNumber: string,
        campusDualHash: string,
        bautzenHash: string,
        mensa: number,
        restart: Function,
        reload: Function,
        changeTheme: Function,
        showTips: boolean,
        ts: number,
        mensaList: Array
    }
}

type State = {
    newCalendarFilter: Array
}

export default class Router extends Component<Props, State> {
    state = {};

    componentDidMount(): void {

    };

    render() {
        const {} = this.state;
        const {Colors, firstStart, currentVersion, newVersion, useDarkTheme, privacyAccept, campusIndex, studyNumber, campusDualHash, bautzenHash, restart, changeTheme, mensa, reload, showTips, ts, mensaList} = this.props.screenProps;

        const AppNavigator = createMaterialBottomTabNavigator(
            {
                Home: {
                    screen: Home,
                    navigationOptions: {
                        tabBarIcon: ({tintColor, focused}) => (
                            <FontAwesome5 name={'home'} size={focused ? 28 : 26} color={tintColor}
                                          style={{width: 50, textAlign: 'center'}}/>
                        )
                    }
                },
                WeekPlan: {
                    screen: WeekPlan,
                    navigationOptions: {
                        tabBarIcon: ({tintColor, focused}) => (
                            <FontAwesome5 name={'calendar-alt'} size={focused ? 28 : 26} color={tintColor}
                                          style={{width: 50, textAlign: 'center'}}/>
                        )
                    }
                },
                Profile: {
                    screen: Profile,
                    navigationOptions: {
                        tabBarIcon: ({tintColor, focused}) => (
                            <FontAwesome5 name={'user-tie'} size={focused ? 28 : 26} color={tintColor}
                                          style={{width: 50, textAlign: 'center'}}/>
                        )
                    }
                },
                Mensa: {
                    screen: Mensa,
                    navigationOptions: {
                        tabBarIcon: ({tintColor, focused}) => (
                            <FontAwesome5 name={'mug-hot'} size={focused ? 28 : 26} color={tintColor}
                                          style={{width: 50, textAlign: 'center'}}/>
                        )
                    }
                },
                Settings: {
                    screen: Settings,
                    navigationOptions: {
                        tabBarIcon: ({tintColor, focused}) => (
                            <FontAwesome5 name={'cogs'} size={focused ? 28 : 26} color={tintColor}
                                          style={{width: 50, textAlign: 'center'}}/>
                        )
                    },
                }
            },
            {
                initialRouteName: 'Home',
                order: ['Profile', 'WeekPlan', 'Home', 'Mensa', 'Settings'],
                labeled: false,
                activeColor: Colors.onPrimary,
                inactiveColor: Colors.onPrimaryTrans,
                barStyle: {backgroundColor: Colors.primaryDark},
                shifting: false
            }
        );

        const AppContainer = createAppContainer(AppNavigator);

        return <View style={{
            flex: 1,
            minWidth: 300,
            minHeight: 500
        }}>
            <AppContainer
                screenProps={{
                Colors,
                firstStart,
                currentVersion,
                newVersion,
                useDarkTheme,
                campusIndex,
                studyNumber,
                campusDualHash,
                bautzenHash,
                restart,
                reload,
                changeTheme,
                mensa,
                showTips,
                ts,
                mensaList
            }}/>

        </View>;
    }
}
