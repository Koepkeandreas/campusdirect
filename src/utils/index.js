import {Linking, ToastAndroid} from "react-native";
import GetFetch from "../Native-Modules/GetFetch";
import * as _ from "lodash";
import {Aes, Pbkdf2} from "@trackforce/react-native-crypto";
import {Buffer} from 'buffer';
import DeviceInfo from 'react-native-device-info';
import dayjs from 'dayjs';

const FETCH_TIMEOUT = 6000;

export const weekdayNames = [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag'
];

export const weekdayNamesShort = [
    'So',
    'Mo',
    'Di',
    'Mi',
    'Do',
    'Fr',
    'Sa'
];

export const monthNames = [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember'
];

export const timeLine = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19];

export const getNearestDate = (plan) => {
    const start = Math.round(new Date().setHours(0, 0, 0) / 1000);
    const end = Math.round(new Date().setHours(23, 59, 59) / 1000);

    let filteredPlan = [];
    if (plan && plan.length > 0) filteredPlan = plan.filter((item) => item.end > end);

    if (plan && filteredPlan && filteredPlan.length > 0) {
        const orderedData = _.orderBy(filteredPlan, ['start'], ['asc']);
        return new Date(_.first(orderedData).start * 1000);
    } else {
        return new Date();
    }
};

export const getDateList = (date) => {
    const dateNow = new Date(date);

    const mon = dateNow.getDate() - dateNow.getDay() + (dateNow.getDay() === 0 ? -6 : 1);
    const monday = new Date(dateNow.setDate(mon));
    const tue = dateNow.getDate() - (dateNow.getDay() - 1) + 1;
    const tuesday = new Date(dateNow.setDate(tue));
    const wen = dateNow.getDate() - (dateNow.getDay() - 1) + 2;
    const wensday = new Date(dateNow.setDate(wen));
    const thu = dateNow.getDate() - (dateNow.getDay() - 1) + 3;
    const thursday = new Date(dateNow.setDate(thu));
    const fri = dateNow.getDate() - (dateNow.getDay() - 1) + 4;
    const friday = new Date(dateNow.setDate(fri));
    const sat = dateNow.getDate() - (dateNow.getDay() - 1) + 5;
    const saturday = new Date(dateNow.setDate(sat));
    const sun = dateNow.getDate() - (dateNow.getDay() - 1) + 6;
    const sunday = new Date(dateNow.setDate(sun));

    return [
        {
            title: 'Mo',
            time: monday,
            day: monday.getDate(),
            today: monday.getDate() === new Date().getDate() && monday.getMonth() === new Date().getMonth()
        }, {
            title: 'Di',
            time: tuesday,
            day: tuesday.getDate(),
            today: tuesday.getDate() === new Date().getDate() && tuesday.getMonth() === new Date().getMonth()
        }, {
            title: 'Mi',
            time: wensday,
            day: wensday.getDate(),
            today: wensday.getDate() === new Date().getDate() && wensday.getMonth() === new Date().getMonth()
        }, {
            title: 'Do',
            time: thursday,
            day: thursday.getDate(),
            today: thursday.getDate() === new Date().getDate() && thursday.getMonth() === new Date().getMonth()
        }, {
            title: 'Fr',
            time: friday,
            day: friday.getDate(),
            today: friday.getDate() === new Date().getDate() && friday.getMonth() === new Date().getMonth()
        }, {
            title: 'Sa',
            time: saturday,
            day: saturday.getDate(),
            today: saturday.getDate() === new Date().getDate() && saturday.getMonth() === new Date().getMonth()
        }, {
            title: 'So',
            time: sunday,
            day: sunday.getDate(),
            today: sunday.getDate() === new Date().getDate() && sunday.getMonth() === new Date().getMonth()
        }
    ];
};

export const wait = (timeout) => {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
};

export const CampusList = [
    {
        "label": "BA Dresden",
        "value": "ba_dd",
        "mensa": 32
    },
    {
        "label": "BA Bautzen",
        "value": "ba_bz",
        "mensa": 42
    },
    {
        "label": "BA Glauchau",
        "value": "ba_gl",
        "mensa": 1000
    }
];

export const DefaultMensaList = [
    {
        "name": "Mensa Johannstadt",
        city: 'Dresden',
        "id": 32
    },
    {
        "name": "Mensa Studienakademie",
        city: 'Bautzen',
        "id": 42
    },
    {
        "name": "Mensa Studienakademie",
        city: 'Glauchau',
        "id": 1000,
        "pdfLink": "https://www.ba-glauchau.de/fileadmin/glauchau/waehrend-des-studium/dokumente/Wochenkarte_Mensa.pdf"
    }
];

export const CampusLinklist = {
    ba_dd: 'https://www.ba-dresden.de/',
    ba_bz: 'https://www.ba-bautzen.de/',
    ba_gl: 'https://www.ba-glauchau.de/'
};

export function _goToURL(url) {
    Linking.canOpenURL(url).then(supported => {
        if (supported) {
            Linking.openURL(url);
        }
    });
}

export const testCampusDualLogin = (matrNr, campusDualHash, onSuccess, onError) => {
    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, du kannst dich noch nicht einloggen.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://selfservice.campus-dual.de/dash/getcp?user=${matrNr}&hash=${campusDualHash}`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const testBautzenLogin = (bautzenHash, onSuccess, onError) => {
    let time = new Date().getTime();
    time = time.toString();
    time = time.slice(0, -3);
    time = parseInt(time);

    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, du kannst dich noch nicht einloggen.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://webapi.ba-bautzen.de/stundenplan/PlanServlet?Format=json&data=${bautzenHash}&start=${time}&end=${time}`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const fetchPlan = (campus, matrNr, campusDualHash, bautzenHash, date, onSuccess, onError) => {
    let didTimeOut = false;

    let dateNow = new Date().getTime();
    if (date !== null) dateNow = date;

    let firstDate = Math.round((dateNow - 1000 * 60 * 60 * 24 * 7) / 1000);
    let lastDate = Math.round((dateNow + 1000 * 60 * 60 * 24 * 7 * 15) / 1000);

    const url = campus === 'ba_bz'
        ? `https://webapi.ba-bautzen.de/stundenplan/PlanServlet?Format=json&data=${bautzenHash}&start=${firstDate}&end=${lastDate}`
        : `https://selfservice.campus-dual.de/room/json?userid=${matrNr}&hash=${campusDualHash}&start=${firstDate}&end=${lastDate}`;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, dir werden somit keine aktuellen Daten angezeigt.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            url,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const fetchReminder = (matrNr, campusDualHash, onSuccess, onError) => {
    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, dir werden somit keine aktuellen Daten angezeigt.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://selfservice.campus-dual.de/dash/getreminders?user=${matrNr}&hash=${campusDualHash}`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const fetchCredits = (matrNr, campusDualHash, onSuccess, onError) => {
    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, dir werden somit keine aktuellen Daten angezeigt.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://selfservice.campus-dual.de/dash/getcp?user=${matrNr}&hash=${campusDualHash}`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const fetchExamStatus = (matrNr, campusDualHash, onSuccess, onError) => {
    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, dir werden somit keine aktuellen Daten angezeigt.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://selfservice.campus-dual.de/dash/getexamstats?user=${matrNr}&hash=${campusDualHash}`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const fetchMensa = (mensaId, date, onSuccess, onError) => {
    const day = dayjs(date).format('DD');
    const month = dayjs(date).format('MM');
    const year = dayjs(date).format('YYYY');

    let didTimeOut = false;

    new Promise(function (resolve, reject) {
        const timeout = setTimeout(function () {
            didTimeOut = true;
            reject('Du hast kein oder schlechtes Internet, dir werden somit keine aktuellen Daten angezeigt.');
        }, FETCH_TIMEOUT);

        GetFetch.request(
            `https://api.studentenwerk-dresden.de/openmensa/v2/canteens/${mensaId}/days/${year}-${month}-${day}/meals`,
            (e) => {
                if (didTimeOut) return;
                reject('Etwas ist schief gelaufen, bitte starte die App neu oder kontaktiere den Support.');
            },
            (res) => {
                clearTimeout(timeout);
                if (!didTimeOut) resolve(res);
            }
        );
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            ToastAndroid.show(err);
            onError();
        });
};

export const encryptStudyNumber = async (studyNumber, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;
    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptStudyNumb');
    const ivBase64 = ivBuffer.toString('hex');

    return await Aes.encrypt(studyNumber + deviceID, key, ivBase64);
};

export const decryptStudyNumber = async (encryptedStudyNumber, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;
    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptStudyNumb');
    const ivBase64 = ivBuffer.toString('hex');

    const decrypted = await Aes.decrypt(encryptedStudyNumber, key, ivBase64);
    return decrypted.replace(deviceID, '');
};

export const encryptCampusDualHash = async (campusDualHash, studyNumber, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;

    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptCampusDua');
    const ivBase64 = ivBuffer.toString('hex');

    return await Aes.encrypt(campusDualHash + studyNumber + deviceID, key, ivBase64);
};

export const decryptCampusDualHash = async (encryptedHash, studyNumber, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;

    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptCampusDua');
    const ivBase64 = ivBuffer.toString('hex');

    let decrypted = await Aes.decrypt(encryptedHash, key, ivBase64);
    decrypted = decrypted.replace(deviceID, '');
    decrypted = decrypted.replace(studyNumber, '');
    return decrypted;
};

export const encryptBautzenHash = async (bautzenHash, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;

    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptBautzenHa');
    const ivBase64 = ivBuffer.toString('hex');

    return await Aes.encrypt(bautzenHash + deviceID, key, ivBase64);
};

export const decryptBautzenHash = async (encryptedHash, ts) => {
    const deviceID = DeviceInfo.getUniqueId();
    const saltBase64 = String(ts).slice(0, 8);
    const iterations = 4096;
    const keyLen = 32;

    const key = await Pbkdf2.hash(deviceID, saltBase64, iterations, keyLen, 'SHA1');
    const ivBuffer = Buffer.from('encryptBautzenHa');
    const ivBase64 = ivBuffer.toString('hex');

    let decrypted = await Aes.decrypt(encryptedHash, key, ivBase64);
    decrypted = decrypted.replace(deviceID, '');
    return decrypted;
};
