import {StyleSheet} from 'react-native';

const light = {
    "primary": "#0d46a0",
    "primaryTrans": "rgba(13, 70, 160, 0.5)",
    "onPrimary": "#FFFFFF",
    "onPrimaryTrans": "rgba(255, 255, 255, 0.6)",
    "primaryLight": "#5471d2",
    "primaryLightTrans": "rgba(84, 113, 210, 0.5)",
    "onPrimaryLight": "#000000",
    "primaryDark": "#002071",
    "primaryDarkTrans": "rgba(0, 32, 113, 0.5)",
    "onPrimaryDark": "#FFFFFF",
    "secondary": "#42a5f5",
    "secondaryTrans": "rgba(66, 165, 245, 0.5)",
    "onSecondary": "#000000",
    "secondaryLight": "#80d6ff",
    "secondaryLightTrans": "rgba(128, 214, 255, 0.4)",
    "onSecondaryLight": "#000000",
    "secondaryDark": "#0077c2",
    "secondaryDarkTrans": "rgba(0, 119, 194, 0.5)",
    "onSecondaryDark": "#FFFFFF",
    "error": "#B00020",
    "onError": "#FFFFFF",
    "background": "#FFFFFF",
    "PlanBackground": "#FFFFFF",
    "onBackground": "#000000",
    "Trans": "rgba(0, 0, 0, 0)",
    "onBackgroundTrans": "rgba(0, 0, 0, 0.6)",
    "border": "#BBB",
    "FilterBackground": "#FFFFFF",
    "onFilterBackground": "#000000",
    "PictureBackground": "#FFFFFF",
    "PlanBackgroundTrans": "rgba(255, 255, 255, 0.5)",
    "BackgroundTrans": "rgba(255, 255, 255, 0.6)",
    "disabled": "#BBB"
};

const dark = {
    "primary": "#0d46a0",
    "primaryTrans": "rgba(13, 70, 160, 0.5)",
    "onPrimary": "#DDDDDD",
    "onPrimaryTrans": "rgba(200, 200, 200, 0.6)",
    "primaryLight": "#5471d2",
    "primaryLightTrans": "rgba(84, 113, 210, 0.5)",
    "onPrimaryLight": "#000000",
    "primaryDark": "#002071",
    "primaryDarkTrans": "rgba(0, 32, 113, 0.5)",
    "onPrimaryDark": "#DDDDDD",
    "secondary": "#42a5f5",
    "secondaryTrans": "rgba(66, 165, 245, 0.5)",
    "onSecondary": "#000000",
    "secondaryLight": "#80d6ff",
    "secondaryLightTrans": "rgba(128, 214, 255, 0.4)",
    "onSecondaryLight": "#000000",
    "secondaryDark": "#0077c2",
    "secondaryDarkTrans": "rgba(0, 119, 194, 0.5)",
    "onSecondaryDark": "#DDDDDD",
    "error": "#B00020",
    "onError": "#DDDDDD",
    "background": "#111111",
    "PlanBackground": "#222222",
    "onBackground": "#DDDDDD",
    "onBackgroundTrans": "rgba(200, 200, 200, 0.6)",
    "border": "#000000",
    "Trans": "rgba(0, 0, 0, 0)",
    "PictureBackground": "#BBBBBB",
    "FilterBackground": "#888888",
    "onFilterBackground": "#FFFFFF",
    "PlanBackgroundTrans": "rgba(50, 50, 50, 0.5)",
    "BackgroundTrans": "rgba(40, 40, 40, 0.6)",
    "disabled": "#BBB"
};

export const useTheme = (darkTheme) => {
    return darkTheme ? dark : light;
};

export const Styles = StyleSheet.create({
    flex: {
        flex: 1
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export const Colors = {};
