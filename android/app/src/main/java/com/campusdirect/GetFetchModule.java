package com.campusdirect;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import java.util.concurrent.TimeUnit;


public class GetFetchModule extends ReactContextBaseJavaModule {



    public GetFetchModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "GetFetch";
    }

    @ReactMethod
    public String request(String url,
                          Callback errorCallback,
                          Callback successCallback) {
        Request request = new Request.Builder()
                .url(url)
                .build();


        OkHttpClient client = null;
        try {
            OkHttpUtil.init(true);
            client = OkHttpUtil.getClient();
        } catch (Exception e) {
            e.printStackTrace();
            errorCallback.invoke(e.getMessage());
        }
        try (Response response = client.newCall(request).execute()) {
            String responseText = response.body().string();
            successCallback.invoke(responseText);
        }
        catch (Exception e){
            e.printStackTrace();
            errorCallback.invoke(e.getMessage());
        }
        return null;
    }
}
